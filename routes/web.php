<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('service', function () {
    return view('service');
});

Route::get('about', function () {
    return view('about');
});

Route::get('account', function () {
    return view('account');
});

Route::get('blog-1', function () {
    return view('blog-1');
});

Route::get('blog-details', function () {
    return view('blog-details');
});

Route::get('blog-large', function () {
    return view('blog-large');
});

Route::get('Campaign-Grid-View', function () {
    return view('Campaign-Grid-View');
});

Route::get('Campaign-List-View', function () {
    return view('Campaign-List-View');
});

Route::get('checkout', function () {
    return view('checkout');
});

Route::get('Eco-System', function () {
    return view('Eco-System');
});

Route::get('error', function () {
    return view('error');
});

Route::get('Events', function () {
    return view('Events');
});

Route::get('faq', function () {
    return view('faq');
});

Route::get('gallery-1', function () {
    return view('gallery-1');
});

Route::get('gallery-2', function () {
    return view('gallery-2');
});

Route::get('gallery-3', function () {
    return view('gallery-3');
});

Route::get('Good-Nature', function () {
    return view('Good-Nature');
});

Route::get('Organic-Living', function () {
    return view('Organic-Living');
});

Route::get('recycling', function () {
    return view('recycling');
});

Route::get('Save-Animals', function () {
    return view('Save-Animals');
});

Route::get('Save-Water', function () {
    return view('Save-Water');
});

Route::get('shop-cart', function () {
    return view('shop-cart');
});

Route::get('shop-single', function () {
    return view('shop-single');
});

Route::get('shop', function () {
    return view('shop');
});

Route::get('single-cause', function () {
    return view('single-cause');
});

Route::get('single-event', function () {
    return view('single-cause');
});

Route::get('single-gallery', function () {
    return view('single-gallery');
});

Route::get('team', function () {
    return view('team');
});

Route::get('testimonial', function () {
    return view('testimonial');
});

Route::get('contact', function () {
    return view('contact');
});





