<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 


</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Single Causes</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="gallery-1">gallery</a>
                </li>
                
                <li>
                    Single Causes
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>


<section class="urgent-cause single-cause style-2">
    <div class="container">

        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="cause-area cause-list-bg sec-padd">
                    <articel class="item clearfix">
                        <figure class="img-box">
                            <img src="images/cause/18.jpg" alt="">                       
                        </figure>
                        
                        <div class="content">
                            <div class="donate"><span>Goal: $40000 </span><br> Raised: $24000</div>
                            <div class="progress-box">
                                <div class="bar">
                                    <div class="bar-inner animated-bar" data-percent="55%"><div class="count-text">55%</div></div>
                                </div>
                            </div>
                            
                        </div>                        
                                
                    </articel><br><br>

                    <div class="section-title style-2">
                        <h3>Help feeding poor demo</h3>
                        <div class="float_right"><button class="thm-btn donate-box-btn style-2">donate Us</button></div>
                    </div>
                    <div class="text">
                        <p>How all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because not know how to pursue pleasure rationally encounter consequences.</p><br>
                        <p>Pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teaching of the great explorer of the truth, the master builder of human happiness one rejects, dislikes, because not know how to pursue pleasure.</p>
                    </div><br><br>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="section-title style-2">
                                <h3>How Can You Help?</h3>
                            </div>
                            <div class="text">
                                <p>Pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer.</p>
                            </div>
                            <ul class="list">
                                <li><i class="fa fa-check-circle"></i>Sponsor meals for 50 children for 1 full year.</li>
                                <li><i class="fa fa-check-circle"></i>Sponsor mid-day meals for one month.</li>
                                <li><i class="fa fa-check-circle"></i>You can donate clothes, blankets and ect...</li>
                                <li><i class="fa fa-check-circle"></i>You can donate food material like rice, veggies.</li>
                                <li><i class="fa fa-check-circle"></i>Join as a volunteers to help poor people.</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="video-image-box">
                                <figure class="gallery_video"><img src="images/cause/19.jpg" alt="" /><a href="https://www.youtube.com/watch?v=8QDazwTY2kM" class=""><span class="overlay-link icon-arrows4"></span></a></figure>
                                
                            </div>

                        </div>
                    </div><br><br>
                    <div class="donator">
                        <div class="section-title style-2">
                            <h3>Latest Donors</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="img-box">
                                    <img src="images/cause/t1.jpg" alt="">
                                    <div class="caption">
                                        <h5>Mark Richarson <br> $1600</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="img-box">
                                    <img src="images/cause/t2.jpg" alt="">
                                    <div class="caption">
                                        <h5>Robert William <br> $1400</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="img-box">
                                    <img src="images/cause/t3.jpg" alt="">
                                    <div class="caption">
                                        <h5>Felicity BNovak <br> $1200</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="img-box">
                                    <img src="images/cause/t4.jpg" alt="">
                                    <div class="caption">
                                        <h5>Robert William <br> $1300</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="share clearfix">
                        <div class="social-box float_left">
                            <span>Share <i class="fa fa-share-alt"></i></span>
                            <ul class="list-inline social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                        <div class="float_right">
                            <a href="#" class="thm-btn style-2 donate-box-btn">donate us</a>
                        </div>
                    </div>

                    <div class="section-title style-2">
                        <h3>leav a reply</h3>
                    </div>

                    <div class="default-form-area">
                        <form id="contact-form" name="contact_form" class="default-form" action="inc/sendmail.php" method="post" novalidate="novalidate">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                    <div class="form-group">
                                        <p>Your Name *</p>
                                        <input type="text" name="form_name" class="form-control" value="" placeholder="" required="" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p>Your Mail *</p>
                                        <input type="email" name="form_email" class="form-control required email" value="" placeholder="" required="" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p>Phone</p>
                                        <input type="text" name="form_phone" class="form-control" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p>Subject</p>
                                        <input type="text" name="form_subject" class="form-control" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <p>Your Comments*</p>
                                        <textarea name="form_message" class="form-control textarea required" placeholder="" aria-required="true"></textarea>
                                    </div>
                                </div>   
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                        <button class="thm-btn" type="submit" data-loading-text="Please wait...">submit now</button>
                                    </div>
                                </div>   

                            </div>
                        </form>
                    </div>


                </div>
                    

            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="blog-sidebar sec-padd">
                    <div class="sidebar_search">
                        <form action="#">
                            <input type="text" placeholder="Search....">
                            <button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>

                    <div class="popular_news">
                        <div class="section-title style-2">
                            <h4>recent post</h4>
                        </div>

                        <div class="popular-post">
                            <div class="item">
                                <div class="post-thumb"><a href="blog-details"><img src="images/blog/thumb3.jpg" alt=""></a></div>
                                <a href="blog-details"><h4>Change the lives of 40 <br> disabled people </h4></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>October 21, 2016 </div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="blog-details"><img src="images/blog/thumb4.jpg" alt=""></a></div>
                                <a href="blog-details"><h4>Gorantalo earthquake <br> Relief Project </h4></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>January 14, 2016</div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="blog-details"><img src="images/blog/thumb5.jpg" alt=""></a></div>
                                <a href="blog-details"><h4>Used equipments can <br> treat poor patients</h4></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>December 17, 2015 </div>
                            </div>
                        </div>
                    </div>

                    <div class="feed-area">
                        <div class="section-title style-2">
                            <h4>Follow On Facebook</h4>
                        </div>
                        <div class="facebook-feed">
                            <figure class="img-box">
                                <img src="images/blog/feedbg.jpg" alt="">
                                <div class="overlay">
                                    <div class="inner-box">
                                        <div class="logo"><img src="images/logo/1.jpg" alt=""></div>
                                        <h4>The Ecogreen</h4>
                                        <div class="like">890 likes</div>
                                    </div>
                                    <div class="link clearfix">
                                        <a href="#" class="float_left"><i class="fa fa-facebook fb-icon"></i>Like page</a>
                                        <a href="contact" class="float_right"><i class="fa fa-envelope mail"></i>contact us</a>
                                    </div>    
                                </div>
                            </figure>
                            <div class="like-people">
                                <p>Be the first of your friends to like this</p>
                                <ul class="list_inline">
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    
                    <div class="sidebar_tags">
                            <div class="section-title style-2">
                            <h4>Product Tags</h4>
                        </div>

                        <ul>
                            <li><a href="#" class="tran3s">Solar</a></li>
                            <li><a href="#" class="tran3s"><b>Ecology</b></a></li>
                            <li><a href="#" class="tran3s">Green House</a></li>
                            <li><a href="#" class="tran3s">Renewable</a></li>
                            <li><a href="#" class="tran3s"><b>Water</b></a>  </li>
                            <li><a href="#" class="tran3s">Wild Animals</a></li>
                            <li><a href="#" class="tran3s"><b>Power Energy</b></a> </li>
                            <li><a href="#" class="tran3s">Ideas</a></li>
                            <li><a href="#" class="tran3s"><b>Recycling</b></a></li>
                            <li><a href="#" class="tran3s">Volunteer</a></li>
                            <li><a href="#" class="tran3s">Clean</a></li>
                        </ul>
                    </div>

                </div>
            </div>       
        </div>
        
    </div>
</section>

@stop

 






</div>
    
</body>
</html>