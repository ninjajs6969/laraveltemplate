<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 

	
</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Event Single</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="event-1">event</a>
                </li>
                
                <li>
                    Event Single
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="event-section style-3">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="single-event sec-padd">
                    <div class="img-box">
                        <img src="images/event/16.jpg" alt="">
                        <div class="countdown-timer">
                            <div class="default-coundown">
                                <div class="countdown time-countdown-two" data-countdown-time="2017/07/07"></div>
                            </div>          
                        </div>
                    </div>
                    <div class="content">
                            
                        <div class="text">
                            <p>Organizer: Tom Maddy</p>
                            <h3>Save Energy In Your Home</h3>
                            <p>Runners interested in participating the event 2017-18 California marathon pleasure and praising pain was born and will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter.</p>
                        </div><br><br>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="event-timeline">
                                    <div class="section-title style-2">
                                        <h3>Event Timeline</h3>
                                    </div>
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i><b>Event Time:</b> Tuesday April 14, 2017 <br>11.00am to 02.00am</li>
                                        <li><i class="fa fa-map-marker"></i><b>Event Venue:</b>  New Grand Street, <br>California, USA-100029</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="section-title style-2">
                                    <h3>Event Schedule</h3>
                                </div>
                                <div class="text">
                                    <p>Pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer.</p>
                                </div>
                                <ul class="list2">
                                    <li><i class="fa fa-check-circle"></i>Sponsor meals for 50 children for 1 full year.</li>
                                    <li><i class="fa fa-check-circle"></i>Sponsor mid-day meals for one month.</li>
                                    <li><i class="fa fa-check-circle"></i>You can donate clothes, blankets and ect...</li>
                                    <li><i class="fa fa-check-circle"></i>You can donate food material like rice, veggies.</li>
                                </ul>
                            </div>      
                        </div><br>
                        <div class="donator">
                            <div class="section-title style-2">
                                <h3>Who Come With Us</h3>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="img-box">
                                        <img src="images/event/t1.jpg" alt="">
                                        <div class="caption">
                                            <h5>Jom Carolena</h5>
                                            <p>Trustee</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="img-box">
                                        <img src="images/event/t2.jpg" alt="">
                                        <div class="caption">
                                            <h5>Robert William </h5>
                                            <p>Founder</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="img-box">
                                        <img src="images/event/t3.jpg" alt="">
                                        <div class="caption">
                                            <h5>Cris Tremain</h5>
                                            <p>Organizer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="img-box">
                                        <img src="images/event/t4.jpg" alt="">
                                        <div class="caption">
                                            <h5>Jack Swepson</h5>
                                            <p>Suapporter</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><br><br>
                        <div class="section-title style-2">
                            <h3>Our Sponsors</h3>
                        </div>
                        <ul class="brand-carousel2">
                            <li><a href="#"><img src="images/event/b1.jpg" alt=""></a></li>
                            <li><a href="#"><img src="images/event/b2.jpg" alt=""></a></li>
                            <li><a href="#"><img src="images/event/b3.jpg" alt=""></a></li>
                            <li><a href="#"><img src="images/event/b4.jpg" alt=""></a></li>
                        </ul>
                    </div>
                    <div class="share clearfix">
                        <div class="social-box float_left">
                            <span>Share <i class="fa fa-share-alt"></i></span>
                            <ul class="list-inline social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                        <div class="float_right">
                            <a href="#" class="thm-btn style-2 donate-box-btn">donate us</a>
                        </div>
                    </div>
                    <div class="section-title style-2">
                        <h3>leav a reply</h3>
                    </div>
                    <div class="default-form-area">
                        <form id="contact-form" name="contact_form" class="default-form" action="inc/sendmail.php" method="post" novalidate="novalidate">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                    <div class="form-group">
                                        <p>Your Name *</p>
                                        <input type="text" name="form_name" class="form-control" value="" placeholder="" required="" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p>Your Mail *</p>
                                        <input type="email" name="form_email" class="form-control required email" value="" placeholder="" required="" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p>Phone</p>
                                        <input type="text" name="form_phone" class="form-control" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p>Subject</p>
                                        <input type="text" name="form_subject" class="form-control" value="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <p>Your Comments*</p>
                                        <textarea name="form_message" class="form-control textarea required" placeholder="" aria-required="true"></textarea>
                                    </div>
                                </div>   
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                        <button class="thm-btn" type="submit" data-loading-text="Please wait...">submit now</button>
                                    </div>
                                </div>   

                            </div>
                        </form>
                    </div>

                </div>
                    

            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="blog-sidebar sec-padd">
                    <div class="event-filter">
                       <div class="section-title style-2">
                            <h4>Event Filter</h4>
                        </div> 
                        <div class="tabs-outer">
                            <!--Tabs Box-->
                            <div class="tabs-box tabs-style-two">
                                <!--Tab Buttons-->
                                <ul class="tab-buttons clearfix">
                                    <li data-tab="#tab-two" class="tab-btn active-btn">ALL</li>
                                    <li data-tab="#tab-one" class="tab-btn">UPCOMING</li>
                                </ul>
                            
                                <!--Tabs Content-->
                                <div class="tabs-content">
                                    <!--Tab / Active Tab-->
                                    
                                    <!--Tab-->
                                    <div class="tab active-tab" id="tab-two" style="display: block;">
                                        <div class="default-form-area all">
                                            <form id="contact-form" name="contact_form" class="default-form style-5" action="inc/sendmail.php" method="post">
                                                <div class="clearfix">

                                                    <div class="form-group">
                                                        <div class="select-box">
                                                            <select class="text-capitalize selectpicker form-control required" name="form_subject" data-style="g-select" data-width="100%">
                                                                <option value="0" selected="">Select venue</option>
                                                                <option value="1">Select venue</option>
                                                                <option value="2">Select venue</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group date">
                                                        <input placeholder="21/08/217" type="text" id="datepicker"><i class="fa fa-calendar"></i>
                                                    </div>


                                                    <div class="form-group">
                                                        <input type="text" placeholder="Search....">
                                                        <button class="tran3s"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                    </div>
                                                    <div class="form-group">
                                                        <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                                        <button class="thm-btn" type="submit" data-loading-text="Please wait...">send message</button>
                                                    </div>
                     

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    
                                    <!--Tab-->
                                    <div class="tab" id="tab-one" style="display: none;">
                                         <div class="default-form-area all">
                                            <form id="contact-form" name="contact_form" class="default-form style-5" action="inc/sendmail.php" method="post">
                                                <div class="clearfix">

                                                    <div class="form-group">
                                                        <div class="select-box">
                                                            <select class="text-capitalize selectpicker form-control required" name="form_subject" data-style="g-select" data-width="100%">
                                                                <option value="0" selected="">Select venue</option>
                                                                <option value="1">Select venue</option>
                                                                <option value="2">Select venue</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group date">
                                                        <input placeholder="21/08/217" type="text" id="datepicker"><i class="fa fa-calendar"></i>
                                                    </div>


                                                    <div class="form-group">
                                                        <input type="text" placeholder="Search....">
                                                        <button class="tran3s"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                    </div>
                                                    <div class="form-group">
                                                        <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                                        <button class="thm-btn" type="submit" data-loading-text="Please wait...">send message</button>
                                                    </div>
                     

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--Tab-->

                                    
                                </div>
                                
                            </div>
                        </div>
                            
                    </div>
                    <div class="event-section2">
                        <div class="section-title style-2">
                            <h4>Popular Events</h4>
                        </div>
                        <div class="event-carousel3">
                            <div class="item">
                                <div class="img-column1">
                                    <figure class="img-holder">
                                        <a href="single-event"><img src="images/resource/12.jpg" alt=""></a>
                                    </figure>
                                </div>
                                <div class="text-column1">
                                    <div class="lower-content">
                                        <a href="single-event"><h4>Conducting Marathon 2017: Run for cancer people</h4></a>
                                        <div class="post-meta"><i class="fa fa-calendar"></i>Started On: 11.00am <br><i class="fa fa-map-marker"></i> New Grand Street, California</div>                  
                                    </div>
                                </div>  
                            </div>
                            <div class="item">
                                <div class="img-column1">
                                    <figure class="img-holder">
                                        <a href="single-event"><img src="images/resource/12.jpg" alt=""></a>
                                    </figure>
                                </div>
                                <div class="text-column1">
                                    <div class="lower-content">
                                        <a href="single-event"><h4>Conducting Marathon 2017: Run for cancer people</h4></a>
                                        <div class="post-meta"><i class="fa fa-calendar"></i>Started On: 11.00am <br><i class="fa fa-map-marker"></i> New Grand Street, California</div>                  
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>

                    <div class="feed-area">
                        <div class="section-title style-2">
                            <h4>Follow On Facebook</h4>
                        </div>
                        <div class="facebook-feed">
                            <figure class="img-box">
                                <img src="images/blog/feedbg.jpg" alt="">
                                <div class="overlay">
                                    <div class="inner-box">
                                        <div class="logo"><img src="images/logo/1.jpg" alt=""></div>
                                        <h4>The Ecogreen</h4>
                                        <div class="like">890 likes</div>
                                    </div>
                                    <div class="link clearfix">
                                        <a href="#" class="float_left"><i class="fa fa-facebook fb-icon"></i>Like page</a>
                                        <a href="contact" class="float_right"><i class="fa fa-envelope mail"></i>contact us</a>
                                    </div>    
                                </div>
                            </figure>
                            <div class="like-people">
                                <p>Be the first of your friends to like this</p>
                                <ul class="list_inline">
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

@stop


 



</div>
    
</body>
</html>