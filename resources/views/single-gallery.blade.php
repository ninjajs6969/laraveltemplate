<!DOCTYPE html>
<html lang="en">
<head>

	<title>Eco Green || Responsive HTML 5 Template</title> 

</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Single Gallery</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="gallery-1">gallery</a>
                </li>
                
                <li>
                    Single Gallery
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="single-gallery sec-padd">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <figure class="img-box"><img src="images/project/34.jpg" alt=""></figure>
                <figure class="img-box"><img src="images/project/35.jpg" alt=""></figure>
                <figure class="img-box"><img src="images/project/36.jpg" alt=""></figure>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <h3><b>Description</b></h3><br>
                <div class="text">
                    <p>When you give to Our Ecogreen, you know your well donation is making a difference. Whether you are supporting one of our Signature Programs or our carefully curated list of all gifts that give more, our professional staff works hard every day.</p><br>
                    <p>Using a high our contrast palette of cool grey & blue against  warm cream wood tones created  dramatic,  place lounging and entertaining.</p>
                </div>
                <ul class="project-info-list">
                    <li>
                        <div class="icon-holder">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <div class="text-holder">
                            <h5>Organizer</h5>
                            <p>Chaz Reubhen</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder">
                            <i class="fa fa-folder-open" aria-hidden="true"></i>
                        </div>
                        <div class="text-holder">
                            <h5>Category</h5>
                            <p>Residential Design</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                        </div>
                        <div class="text-holder">
                            <h5>Start Date</h5>
                            <p>17th Dec 2015</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                        </div>
                        <div class="text-holder">
                            <h5>End Date</h5>
                            <p>21st Feb 2016</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder">
                            <i class="fa fa-link" aria-hidden="true"></i>
                        </div>
                        <div class="text-holder">
                            <h5>Showcase Link</h5>
                            <p>www.demo.com</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-holder">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="text-holder">
                            <h5>Location</h5>
                            <p>Newyork City, USA</p>
                        </div>
                    </li>
                </ul>
                <div class="share-project">
                    <div class="title">
                        <h5>share this project</h5>
                    </div>
                    <div class="social-share">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="page_pagination2">    
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="button prev pull-left">
                        <a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp;&nbsp;Prev</a>    
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="icon-holder text-center">
                        <a href="#">
                            <i class="fa fa-th" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="button next pull-right">
                        <a href="#">Next &nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></a>    
                    </div>
                </div>
            </div>
                
        </div>
    </div>
</section>

@stop

 



</div>
    
</body>
</html>