<!DOCTYPE html>
<html lang="en">
<head>
   
	<title>Eco Green || Responsive HTML 5 Template</title> 

	

</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')

@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Contact Us</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="about">About Us</a>
                </li>
                
                <li>
                    Contact Us
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>

<!--Start contact area-->
<section class="get-touch-area">
    <div class="container">
        <div class="sec-title text-center">
            <h1>Get Touch With Us</h1>
            <span class="border"></span>
            <p>We recently helped a small business grow from break-even to over $1m profit in less than 2 years.
            Please find below contact details and contact us today!</p>
        </div>
        <div class="row">
            <!--Start single item-->
            <div class="col-md-4">
                <div class="single-item hvr-grow-shadow text-center">
                    <div class="icon-holder">
                        <span class="icon-signs2"></span>    
                    </div> 
                    <div class="text-holder">
                        <h3>Come to See Us</h3>
                        <span class="border"></span>
                        <p>241/84 Theme Name Discover Street <br>New York, NY 10012, USA</p>
                    </div>  
                </div>
            </div>
            <!--End single item-->
            <!--Start single item-->
            <div class="col-md-4">
                <div class="single-item hvr-grow-shadow text-center">
                    <div class="icon-holder">
                        <span class="icon-technology"></span>    
                    </div> 
                    <div class="text-holder">
                        <h3>Quick Contact</h3>
                        <span class="border"></span>
                        <p>Ph: +321 456 78 9012 <br>Email: <a href="#">Info@Wesupportyou.com</a></p>
                    </div>  
                </div>
            </div>
            <!--End single item-->
            <!--Start single item-->
            <div class="col-md-4">
                <div class="single-item hvr-grow-shadow text-center">
                    <div class="icon-holder">
                        <span class="icon-clock"></span>    
                    </div> 
                    <div class="text-holder">
                        <h3>Office Schedule</h3>
                        <span class="border"></span>
                        <p>Monday to Saturday: 09.00am to 18.00pm <br>Sunday <a href="#">: Closed</a></p>
                    </div>  
                </div>
            </div>
            <!--End single item-->           
        </div>
    </div>
</section>
<!--End contact area-->

<!--Start contact form area-->
<section class="contact-form-area">
    <div class="container">
        <div class="row">
           
            <div class="col-md-8">
                <div class="default-form-area">
                    <form id="contact-form" name="contact_form" class="default-form" action="inc/sendmail.php" method="post">
                        <div class="row clearfix">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                
                                <div class="form-group">
                                    <input type="text" name="form_name" class="form-control" value="" placeholder="Your Name *" required="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input type="email" name="form_email" class="form-control required email" value="" placeholder="Your Mail *" required="">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input type="text" name="form_phone" class="form-control" value="" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input type="text" name="form_subject" class="form-control" value="" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <textarea name="form_message" class="form-control textarea required" placeholder="Your Message...."></textarea>
                                </div>
                            </div>   
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group center">
                                    <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                    <button class="thm-btn style-2" type="submit" data-loading-text="Please wait...">send message</button>
                                </div>
                            </div>   

                        </div>
                    </form>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-right-box text-center">
                    <div class="img-box">
                        <img src="images/resource/contact-form-right.png" alt="Awesome Image">
                    </div>
                    <h4>Brown Angelino</h4>
                    <span>Hr Manager</span>
                    <p>How all this mistaken idea of denouncing <br>pleasure and praising pain was born and I will <br>complete account of the system.</p>
                    <div class="border"></div>
                    <ul class="social-links">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>   
                </div>    
            </div>
                
        </div>
    </div>
</section>
<!--End contact form area-->

<section class="home-google-map">

    <div 
        class="google-map" 
        id="contact-google-map" 
        data-map-lat="40.108833" 
        data-map-lng="-74.782104" 
        data-icon-path="images/logo/map-marker.png"
        data-map-title="Chester"
        data-map-zoom="11" >

    </div>

</section>

@stop

    <!-- google map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script> 
    <script src="js/gmap.js"></script>


    <script id="map-script" src="js/default-map.js"></script>

</div>
    
</body>
</html>