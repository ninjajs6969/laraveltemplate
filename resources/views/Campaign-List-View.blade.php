<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 

</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Campaigns List View</h1>
        </div>
    </div>
</div>

<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="#">Pages</a>
                </li>
                
                <li>
                    Campaigns List View
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>


<section class="urgent-cause2 list-style style-2">
    <div class="container">

        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="cause-list cause-list-bg sec-padd">
                    <articel class="item clearfix">
                        <figure class="img-box">
                            <img src="images/cause/7.jpg" alt="">
                            <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                        </figure>
                        
                        <div class="content">
                        
                            <div class="text center">
                                <a href="single-cause"><h4 class="title">Wind Power Grows Up</h4></a>
                                <p>Fusce et augue placerat, dictum velit sit amet, ege <br>stasuna. cras aliquam pretium ornare liquam <br>metus. Aenean venenatis sodales...</p>               
                            </div>
                            <div class="progress-box">
                                <div class="bar">
                                    <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                                </div>
                            </div>
                            <div class="donate clearfix">
                                <div class="donate float_left"><span>Goal: $92000 </span></div>
                                <div class="donate float_right">Raised: $69000</div>
                            </div>
                            
                        </div>                     
                                
                    </articel>
                    <articel class="item clearfix">
                        <figure class="img-box">
                            <img src="images/cause/8.jpg" alt="">
                            <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                        </figure>
                        
                        <div class="content">
                        
                            <div class="text center">
                                <a href="single-cause"><h4 class="title">Save White Tiger</h4></a>
                                <p>Phasellus cursus nunc arcu, eget sollicitudin mi <br>lacinia tempus. Donec ligula turpis, egestas <br> at volutpat no liquam...</p>               
                            </div>
                            <div class="progress-box">
                                <div class="bar">
                                    <div class="bar-inner animated-bar" data-percent="75%"><div class="count-text">75%</div></div>
                                </div>
                            </div>
                            <div class="donate clearfix">
                                <div class="donate float_left"><span>Goal: $69000 </span></div>
                                <div class="donate float_right">Raised: $48000</div>
                            </div>
                            
                        </div>                     
                                
                    </articel>
                    <articel class="item clearfix">
                        <figure class="img-box">
                            <img src="images/cause/9.jpg" alt="">
                            <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                        </figure>
                        
                        <div class="content">
                        
                            <div class="text center">
                                <a href="single-cause"><h4 class="title">Go Green Movement</h4></a>
                                <p>Fusce et augue placerat, dictum velit sit amet, ege <br>stasuna. cras aliquam pretium ornare liquam <br>metus. Aenean venenatis sodales...</p>               
                            </div>
                            <div class="progress-box">
                                <div class="bar">
                                    <div class="bar-inner animated-bar" data-percent="68%"><div class="count-text">68%</div></div>
                                </div>
                            </div>
                            <div class="donate clearfix">
                                <div class="donate float_left"><span>Goal: $54000 </span></div>
                                <div class="donate float_right">Raised: $24000</div>
                            </div>
                            
                        </div>                     
                                
                    </articel>
                    <articel class="item clearfix">
                        <figure class="img-box">
                            <img src="images/cause/10.jpg" alt="">
                            <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                        </figure>
                        
                        <div class="content">
                        
                            <div class="text center">
                                <a href="single-cause"><h4 class="title">40,000 Elephants</h4></a>
                                <p>Phasellus cursus nunc arcu, eget sollicitudin mi <br>lacinia tempus. Donec ligula turpis, egestas <br> at volutpat no liquam...</p>               
                            </div>
                            <div class="progress-box">
                                <div class="bar">
                                    <div class="bar-inner animated-bar" data-percent="68%"><div class="count-text">68%</div></div>
                                </div>
                            </div>
                            <div class="donate clearfix">
                                <div class="donate float_left"><span>Goal: $92000 </span></div>
                                <div class="donate float_right">Raised: $69000</div>
                            </div>
                            
                        </div>                     
                                
                    </articel>
                    <articel class="item clearfix">
                        <figure class="img-box">
                            <img src="images/cause/11.jpg" alt="">
                            <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                        </figure>
                        
                        <div class="content">
                        
                            <div class="text center">
                                <a href="single-cause"><h4 class="title">Recycling Bamboo</h4></a>
                                <p>Etiam vitae leo et diam pellentesque portaed <br>eleifend ultricies risu, vel rutrum era commodo ut <br>Praesent finibus congue euismod...</p>               
                            </div>
                            <div class="progress-box">
                                <div class="bar">
                                    <div class="bar-inner animated-bar" data-percent="75%"><div class="count-text">75%</div></div>
                                </div>
                            </div>
                            <div class="donate clearfix">
                                <div class="donate float_left"><span>Goal: $78000 </span></div>
                                <div class="donate float_right">Raised: $49000</div>
                            </div>
                            
                        </div>                     
                                
                    </articel>
                    <articel class="item clearfix">
                        <figure class="img-box">
                            <img src="images/cause/12.jpg" alt="">
                            <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                        </figure>
                        
                        <div class="content">
                        
                            <div class="text center">
                                <a href="single-cause"><h4 class="title">Go Green Movements</h4></a>
                                <p>Etiam vitae leo et diam pellentesque portaed <br>eleifend ultricies risu, vel rutrum era commodo ut <br>Praesent finibus congue euismod...</p>                
                            </div>
                            <div class="progress-box">
                                <div class="bar">
                                    <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                                </div>
                            </div>
                            <div class="donate clearfix">
                                <div class="donate float_left"><span>Goal: $54000 </span></div>
                                <div class="donate float_right">Raised: $24000</div>
                            </div>
                            
                        </div>                     
                                
                    </articel>
                    <ul class="page_pagination center">
                        <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                        <li><a href="#" class="active tran3s">1</a></li>
                        <li><a href="#" class="tran3s">2</a></li>
                        <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                    

            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="blog-sidebar sec-padd">
                    <div class="sidebar_search">
                        <form action="#">
                            <input type="text" placeholder="Search....">
                            <button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>

                    <div class="category-style-one">
                        <div class="section-title style-2">
                            <h4>Categories</h4>
                        </div>
                        <ul class="list">
                            <li><a href="#">Childrens Education</a></li>
                            <li><a href="#">Community Ecogreen</a></li>
                            <li><a href="#">Poor People Life Saving</a></li>
                            <li><a href="#">Environmental Saving</a></li>
                            <li><a href="#">Uncategorized Post</a></li>
                        </ul>
                    </div> 

                    <div class="popular_news">
                        <div class="section-title style-2">
                            <h4>recent post</h4>
                        </div>

                        <div class="popular-post">
                            <div class="item">
                                <div class="post-thumb"><a href="blog-details"><img src="images/blog/thumb3.jpg" alt=""></a></div>
                                <a href="blog-details"><h4>Change the lives of 40 <br> disabled people </h4></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>October 21, 2016 </div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="blog-details"><img src="images/blog/thumb4.jpg" alt=""></a></div>
                                <a href="blog-details"><h4>Gorantalo earthquake <br> Relief Project </h4></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>January 14, 2016</div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="blog-details"><img src="images/blog/thumb5.jpg" alt=""></a></div>
                                <a href="blog-details"><h4>Used equipments can <br> treat poor patients</h4></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>December 17, 2015 </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="event-section2">
                        <div class="section-title style-2">
                            <h4>Upcoming Events</h4>
                        </div>
                        <div class="event-carousel3">
                            <div class="item">
                                <div class="img-column">
                                    <figure class="img-holder">
                                        <a href="single-cause"><img src="images/resource/12.jpg" alt=""></a>
                                    </figure>
                                </div>
                                <div class="text-column">
                                    <div class="lower-content">
                                        <a href="single-cause"><h4>Conducting Marathon 2017: Run for cancer people</h4></a>
                                        <div class="post-meta"><i class="fa fa-calendar"></i>Started On: 11.00am <br><i class="fa fa-map-marker"></i> New Grand Street, California</div>                  
                                    </div>
                                </div>  
                            </div>
                            <div class="item">
                                <div class="img-column">
                                    <figure class="img-holder">
                                        <a href="single-cause"><img src="images/resource/12.jpg" alt=""></a>
                                    </figure>
                                </div>
                                <div class="text-column">
                                    <div class="lower-content">
                                        <a href="single-cause"><h4>Conducting Marathon 2017: Run for cancer people</h4></a>
                                        <div class="post-meta"><i class="fa fa-calendar"></i>Started On: 11.00am <br><i class="fa fa-map-marker"></i> New Grand Street, California</div>                  
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>

                    <div class="feed-area">
                        <div class="section-title style-2">
                            <h4>Follow On Facebook</h4>
                        </div>
                        <div class="facebook-feed">
                            <figure class="img-box">
                                <img src="images/blog/feedbg.jpg" alt="">
                                <div class="overlay">
                                    <div class="inner-box">
                                        <div class="logo"><img src="images/logo/1.jpg" alt=""></div>
                                        <h4>The Ecogreen</h4>
                                        <div class="like">890 likes</div>
                                    </div>
                                    <div class="link clearfix">
                                        <a href="#" class="float_left"><i class="fa fa-facebook fb-icon"></i>Like page</a>
                                        <a href="contact" class="float_right"><i class="fa fa-envelope mail"></i>contact us</a>
                                    </div>    
                                </div>
                            </figure>
                            <div class="like-people">
                                <p>Be the first of your friends to like this</p>
                                <ul class="list_inline">
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                    <li><a href="#"><img src="images/blog/p1.jpg" alt=""></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>


                </div>
            </div>       
        </div>
        
    </div>
</section>

@stop




</div>
    
</body>
</html>