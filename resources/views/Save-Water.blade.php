<!DOCTYPE html>
<html lang="en">
<head>
   
    <title>Eco Green || Responsive HTML 5 Template</title> 

   
</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Services Single</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li> 
                <li>
                    <a href="service">Service</a>
                </li>                
                <li>
                    Services Single
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>

<section class="default-section sec-padd">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-12">
                <div class="default-sidebar">
                    <ul class="service-catergory">
                        <li><a href="recycling">Recycling</a></li>
                        <li><a href="Eco-System">Eco System</a></li>
                        <li class="active"><a href="Save-Water">Save Water</a></li>
                        <li><a href="Save-Animals">Save Animals</a></li>
                        <li><a href="Organic-Living">Organic Living</a></li>
                        <li><a href="Good-Nature">Good Nature</a></li>
                    </ul>
                    <div class="link"><a href="#" class="thm-btn style-2">Become a Volunteer</a></div><br><br>
                    <div class="event-filter">
                        <h3>Make Donation</h3>
                        <p>Sposers this campaing with any <br>amount you think will help.</p>
                        <div class="default-form-area all">
                            <form id="contact-form" name="contact_form" class="default-form style-5" action="inc/sendmail.php" method="post">
                                <div class="clearfix">
                                    <h5>Donation Amount</h5>
                                    <div class="form-group date">
                                        <input placeholder="$250" type="text">
                                    </div>

                                    <h5>Personal Details</h5>
                                    <div class="form-group">
                                        <input type="text" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="Email Name">
                                    </div>
                                    <div class="form-group">
                                        <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                        <div class="center">
                                            <button class="thm-btn" type="submit" data-loading-text="Please wait...">send message</button>
                                        </div>
                                    </div>
     

                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-12">
                <div class="single-service">
                    <div class="row mb-30">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="img-box">
                                <img src="images/service/1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <h2>ECO System</h2><br>
                            <div class="text">
                                <p>There anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage.</p><br>
                                <p>Who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces annoying consequences resultant pleasure.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-30">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="img-box">
                                <img src="images/service/2.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <h2>Who Are We Working For?</h2><br>
                            <div class="text">
                                <p>Occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever physical exercise, except to obtain some advantage.</p>
                            </div><br>
                            <ul class="default-list">
                                <li><i class="fa fa-check-circle"></i>Sponsor meals for 50 children for 1 full year.</li>
                                <li><i class="fa fa-check-circle"></i>Sponsor mid-day meals for one month.</li>
                                <li><i class="fa fa-check-circle"></i>You can donate clothes, blankets and ect...</li>
                                <li><i class="fa fa-check-circle"></i>You can donate food material like rice, veggies.</li>
                            </ul>
                        </div>
                    </div><br><br>

                    <h2>ECO System Services</h2><br><br>
                    <!--Tabs Box-->
                    <div class="tabs-box tabs-style-one">
                        <!--Tab Buttons-->
                        <ul class="tab-buttons clearfix">
                            <li data-tab="#tab-one" class="tab-btn active-btn">Service 01</li>
                            <li data-tab="#tab-two" class="tab-btn">Service 02</li>
                            <li data-tab="#tab-three" class="tab-btn">Service 03</li>
                        </ul>
                    
                        <!--Tabs Content-->
                        <div class="tabs-content">
                            <!--Tab / Active Tab-->
                            <div class="tab active-tab" id="tab-one">
                                <div class="text-content">
                                    <div class="text"><p>Philosophy of form following function prevalent in modernism. These designs represent the ideals of cutting excess, practicality and an absence of decoration. These forms of lamps are visually light and follow minimalist principles of design which are influenced by architectural concepts like the cantilever.</p></div>
                                </div>
                                <div class="skills">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="65"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Provisioning</p>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="80"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Supporting</p>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="55"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Regulating</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--Tab-->
                            <div class="tab" id="tab-two">
                                <div class="text-content">
                                    <div class="text"><p>Philosophy of form following function prevalent in modernism. These designs represent the ideals of cutting excess, practicality and an absence of decoration. These forms of lamps are visually light and follow minimalist principles of design which are influenced by architectural concepts like the cantilever.</p></div>
                                </div>
                                <div class="skills">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="65"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Provisioning</p>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="80"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Supporting</p>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="55"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Regulating</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--Tab-->
                            <div class="tab" id="tab-three">
                                <div class="text-content">
                                    <div class="text"><p>Philosophy of form following function prevalent in modernism. These designs represent the ideals of cutting excess, practicality and an absence of decoration. These forms of lamps are visually light and follow minimalist principles of design which are influenced by architectural concepts like the cantilever.</p></div>
                                </div>
                                <div class="skills">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="65"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Provisioning</p>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="80"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Supporting</p>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12 item text-center">
                                            <div class="pie-title-center knob" data-percent="55"> 
                                                <span class="pie-value"></span> 
                                            </div>
                                            <p>Regulating</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

@stop





</div>
    
</body>
</html>