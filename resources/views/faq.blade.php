<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 

 

</head>
<body>

<div class="boxed_wrapper">
@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>FAQ'S</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="#">Pages</a>
                </li>
                
                <li>
                    FAQ's
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="faq-section sec-padd2">
    <div class="container">
        <div class="row">
            
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h2>FAQ’s Style 01</h2>
                </div>
                <div class="accordion-box style-one">
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What do you mean by item and end product</p>
                            <div class="toggle-icon">
                                <span class="plus fa fa-chevron-circle-right"></span><span class="minus fa fa-chevron-circle-down"></span>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals. 
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What are some examples of permitted end products?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">Am I allowed to modify the item that I purchased?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What does non-exclusive mean?</p>
                            <div class="toggle-icon">
                                <span class="plus fa fa-chevron-circle-right"></span><span class="minus fa fa-chevron-circle-down"></span>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals. 
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What is a single application?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What do you mean by item and end product</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>

                </div>

                    
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h2>FAQ’s Style 02</h2>
                </div>
                <div class="accordion-box style-three">
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What do you mean by item and end product</p>
                            <div class="toggle-icon">
                                <span class="plus fa fa-plus"></span><span class="minus fa fa-minus"></span>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals. 
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">Am I allowed to modify the item that I purchased?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What are some examples of permitted end products?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What do you mean by item and end product</p>
                            <div class="toggle-icon">
                                <span class="plus fa fa-plus"></span><span class="minus fa fa-minus"></span>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals. 
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What is a single application?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What does non-exclusive mean?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-plus"></i><i class="minus fa fa-minus"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>

                </div>

                    
            </div>

        </div>
    </div>
</section>

@stop



 






</div>
    
</body>
</html>