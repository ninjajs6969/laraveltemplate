<!DOCTYPE html>
<html lang="en">
<head>
   
    <title>Eco Green || Responsive HTML 5 Template</title> 

   
</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Shopping cart</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li> 
                <li>
                    <a href="shop">shop</a>
                </li>                
                <li>
                    Shopping cart
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="cart-section sec-padd-top">
    <div class="container">
        
        <!--Cart Outer-->
        <div class="cart-outer">
            <div class="table-outer">
                <table class="cart-table">
                    <thead class="cart-header">
                        <tr>
                            <th class="prod-column">Product</th>
                            <th>&nbsp;</th>
                            <th>Quantity</th>
                            <th>Avalability</th>
                            <th class="price">Price</th>
                            <th>Total</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td colspan="2" class="prod-column">
                                <div class="column-box">
                                    <figure class="prod-thumb"><a href="#"><img src="images/shop/11.jpg" alt=""></a></figure>
                                    <h3 class="prod-title padd-top-20">The Innovators Dilemma</h3>
                                </div>
                            </td>
                            <td class="qty"><input class="quantity-spinner" type="text" value="1" name="quantity"></td>
                            <td class="unit-price"><div class="available-info"><span class="icon fa fa-check"></span> Item(s) <br>Avilable Now</div></td>
                            <td class="price">$44.99</td>
                            <td class="sub-total">$44.99</td>
                            <td class="remove"><a href="#" class="remove-btn"><span class="icon-multiply"></span></a></td>
                        </tr>
                        
                        <tr>
                            <td colspan="2" class="prod-column">
                                <div class="column-box">
                                    <figure class="prod-thumb"><a href="#"><img src="images/shop/12.jpg" alt=""></a></figure>
                                    <h3 class="prod-title padd-top-20">Good to Great Look</h3>
                                </div>
                            </td>
                            <td class="qty"><input class="quantity-spinner" type="text" value="2" name="quantity"></td>
                            <td class="unit-price"><div class="available-info"><span class="icon fa fa-check"></span> Item(s) <br>Avilable Now</div></td>
                            <td class="price">$44.99</td>
                            <td class="sub-total">$44.99</td>
                            <td class="remove"><a href="#" class="remove-btn"><span class="icon-multiply"></span></a></td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>            
            <div class="update-cart-box clearfix">
                <div class="pull-left">
                    <div class="apply-coupon clearfix">
                        <div class="form-group clearfix">
                           <input type="text" name="coupon-code" value="" placeholder="Enter Coupon Code...">
                        </div>
                        <div class="form-group clearfix">
                            <button type="button" class="thm-btn ">Apply Coupon</button>
                        </div>
                    </div>
                </div>
                
                <div class="pull-right">
                    <button type="button" class="thm-btn update-cart">Update Cart</button> &emsp;
                </div>
                
            </div>

            
            
            <div class="row clearfix sec-pad">
                <div class="column col-md-6 col-sm-12 col-xs-12">
                    <div class="estimate-form">
                        <div class="sec-title">        
                            <h3>Calculate Shipping</h3>
                        </div>
                        <form method="post" action="#" class="default-form">
                            <div class="row clearfix">
                                <!--Form Group-->
                                <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                   
                                    <div class="select-box">
                                        <select class="text-capitalize selectpicker form-control required" name="form_subject" data-style="g-select" data-width="100%">
                                            <option value="0" selected="">United Kingdom (UK)</option>
                                            <option value="1">Pakistan</option>
                                            <option value="2">INDIA</option>
                                        </select>
                                    </div>

                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="select-box">
                                        <select class="text-capitalize selectpicker form-control required" name="form_subject" data-style="g-select" data-width="100%">
                                            <option value="0" selected="">State / Country</option>
                                            <option>Maharshtra</option>
                                            <option>NY</option>
                                            <option>ALabama</option>
                                            <option>Mexico</option>
                                        </select>
                                    </div>
                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="code" value="" placeholder="Zip / Postal Code">
                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="thm-btn">Update Totals</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                <div class="column cart-total col-md-6 col-sm-12 col-xs-12">
                    <div class="sec-title medium ">        
                        <h3>Cart Totals</h3>
                    </div>                    
                    <!--Totals Table-->
                    <ul class="totals-table">
                        <li class="clearfix"><span class="col col-title">Cart Subtotal</span><span class="col">$146.00</span></li>
                        <li class="clearfix"><span class="col col-title">Shipping and Handling</span><span class="col">Free Shipping</span></li>
                        <li class="clearfix"><span class="col col-title">Order Total</span><span class="col">$146.00</span></li>
                    </ul>
                    
                    <div class="margin-top-30 text-right"><a href="checkout.php" class="thm-btn inverse">Proceed to Checkout</a></div>
                </div>
                
            </div>
            
            
        </div>
        
    </div>
</section>

@stop






</div>
    
</body>
</html>