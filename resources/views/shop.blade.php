<!DOCTYPE html>
<html lang="en">
<head>
    
    <title>Eco Green || Responsive HTML 5 Template</title> 


</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Shop Products</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>                
                <li>
                    Shop
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>


<div class="shop sec-padd">
    <div class="container">
        <div class="row">

            <div class="col-md-3 col-sm-12 col-xs-12 sidebar_styleTwo">
                <div class="wrapper shop-sidebar">
                    <div class="sidebar_search">
                        <form action="#">
                            <input type="text" placeholder="Search....">
                            <button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div> <br><br><!-- End of .sidebar_styleOne -->

                    <div class="category-style-one">
                        <div class="section-title style-2">
                            <h4>Categories</h4>
                        </div>
                        <ul class="list">
                            <li><a href="#">Men’s Clothing</a></li>
                            <li><a href="#">Womens’s Clothing</a></li>
                            <li><a href="#">Solar Panel</a></li>
                            <li><a href="#">Sun Glasses</a></li>
                            <li><a href="#">Bonsai Trees</a></li>
                        </ul>
                    </div>

                    <div class="price_filter wow fadeInUp">
                        <div class="section-title style-2">
                            <h4>Filter By Price</h4>
                        </div>
                        <div class="single-sidebar price-ranger">
                            <div id="slider-range"></div>
                            <div class="ranger-min-max-block">
                                <input type="submit" value="Filter">
                                <span>Price:</span>
                                <input type="text" readonly class="min"> 
                                <span>-</span>
                                <input type="text" readonly class="max">
                            </div>
                        </div> <!-- /price-ranger -->
                    </div> <!-- /price_filter -->

                    <div class="best_sellers clearfix wow fadeInUp">
                        <div class="section-title style-2">
                            <h4>Best Sellers</h4>
                        </div>
                        <div class="best-selling-area">
                            <div class="best_selling_item clearfix border">
                                <div class="img_holder float_left">
                                    <a href="shop-single"><img src="images/shop/11.jpg" alt="image"></a>
                                </div> <!-- End of .img_holder -->

                                <div class="text float_left">
                                    <a href="shop-single"><h4>The Innovators</h4></a>
                                    <span>$34.99</span>
                                    <ul>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                    </ul>
                                </div> <!-- End of .text -->
                            </div> <!-- End of .best_selling_item -->

                            <div class="best_selling_item clearfix border">
                                <div class="img_holder float_left">
                                    <a href="shop-single"><img src="images/shop/12.jpg" alt="image"></a>
                                </div> <!-- End of .img_holder -->

                                <div class="text float_left">
                                    <a href="shop-single"><h4>Good to Great</h4></a>
                                    <span>$24.00</span>
                                    <ul>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    </ul>
                                </div> <!-- End of .text -->
                            </div> <!-- End of .best_selling_item -->

                            <div class="best_selling_item clearfix">
                                <div class="img_holder float_left">
                                    <a href="shop-single"><img src="images/shop/13.jpg" alt="image"></a>
                                </div> <!-- End of .img_holder -->

                                <div class="text float_left">
                                    <a href="shop-single"><h4>Built to Last</h4></a>
                                    <span>$20.00</span>
                                    <ul>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                    </ul>
                                </div> <!-- End of .text -->
                            </div> <!-- End of .best_selling_item -->
                        </div>
                            
                    </div> <!-- End of .best_sellers -->


                    <div class="sidebar_tags wow fadeInUp">
                        <div class="section-title style-2">
                            <h4>Product Tags</h4>
                        </div>

                        <ul>
                            <li><a href="#" class="tran3s">T-Shirts</a></li>
                            <li><a href="#" class="tran3s"><b>Clothing</b></a></li>
                            <li><a href="#" class="tran3s">Hoodies</a></li>
                            <li><a href="#" class="tran3s">Popular Brands</a></li>
                            <li><a href="#" class="tran3s"><b>Ninja</b></a>  </li>
                            <li><a href="#" class="tran3s">Books</a></li>
                            <li><a href="#" class="tran3s"><b>Music</b></a> </li>
                            <li><a href="#" class="tran3s">Accessories</a></li>
                            <li><a href="#" class="tran3s">Cookware</a></li>
                            <li><a href="#" class="tran3s"><b>Postcard</b></a></li>
                            <li><a href="#" class="tran3s">Culinary</a></li>
                        </ul>
                    </div> <!-- End of .sidebar_tags -->

                </div> <!-- End of .wrapper -->
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/1.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">The Innovators</a></h4>
                                    <div class="item-price">$34.99</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/2.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Win Your Friends</a></h4>
                                    <div class="item-price">$29.99</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/3.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Business Adventures</a></h4>
                                    <div class="item-price">$24.99</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/4.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Good to Great</a></h4>
                                    <div class="item-price">$29.00</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/5.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Built to Last</a></h4>
                                    <div class="item-price">$24.99</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/6.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Art of the Start</a></h4>
                                    <div class="item-price">$34.99</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/7.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Business Adventures</a></h4>
                                    <div class="item-price">$24.99</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/8.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Lords of Strategy</a></h4>
                                    <div class="item-price">$34.99</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                    
                        <div class="single-shop-item">
                            <div class="img-box">
                                <a href="shop-cart"><img src="images/shop/9.jpg" alt="Awesome Image"></a>
                                <figcaption class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="shop-single"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </figcaption>
                            </div><!-- /.img-box -->
                            <div class="content-box">  
                                <div class="inner-box">
                                    <h4><a href="shop-cart">Win Your Friends</a></h4>
                                    <div class="item-price">$29.00</div>
                                </div> 
                                <div class="price-box">
                                    <div class="clearfix">
                                        <div class="float_left">
                                            <a href="shop-cart" class="cart-btn"><i class="fa fa-shopping-cart"></i>Add To Cart</a>
                                        </div>
                                        <div class="float_right">
                                            <div class="rating">
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span> 
                                                <span class="fa fa-star"></span>
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ul class="page_pagination center">
                    <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="active tran3s">1</a></li>
                    <li><a href="#" class="tran3s">2</a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                </ul>
            </div>

                    
        </div>
    </div> 
</div> 

@stop







</div>
    
</body>
</html>