<!DOCTYPE html>
<html lang="en">
<head>

	<title>Eco Green || Responsive HTML 5 Template</title> 


</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Meet Our Team</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="about">About Us</a>
                </li>
                
                <li>
                    Meet Our Team
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>


<section class="our-team sec-padd2">
    <div class="container">
        <div class="section-title">
            <h2>Meat Our team</h2>
            <a href="#" class="thm-btn style-2">VIEW ALL</a>
        </div>  
        <div class="row">
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t1.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Felicity BNovak</h4></a>
                        <p>CEO & Founder</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t2.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Mark Richarson</h4></a>
                        <p>Board of Trustee</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t3.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Jom Caraleno</h4></a>
                        <p>Board of Trustee</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t4.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Asahtan Marsh</h4></a>
                        <p>Board of Advisor</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>

            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t5.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Jack Sweapson</h4></a>
                        <p>Board of Advisor</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t6.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Chris Tremain</h4></a>
                        <p>Organisor</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t7.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Astley Fletcher</h4></a>
                        <p>Manager</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t8.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>William Linnette</h4></a>
                        <p>Accountant</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>

            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t9.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Astley Fletcher</h4></a>
                        <p>Supporting Staff</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t10.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>William Linnette</h4></a>
                        <p>Supporting Staff</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t11.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Jack Sweapson</h4></a>
                        <p>Volunteer</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                    <figure class="img-box">
                        <a href="#"><img src="images/team/t12.jpg" alt=""></a>
                        <div class="overlay">
                            <div class="inner-box">
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                                </ul>
                            </div>
                                
                        </div>
                    </figure>
                    <div class="author-info">
                        <a href="#"><h4>Chris Tremain</h4></a>
                        <p>Volunteer</p>
                        <ul>
                            <li><i class="fa fa-phone-square"></i>Phone: +123-456-7890</li>
                            <li><i class="fa fa-envelope-square"></i><a href="#">Felicity@Experts.com</a></li>
                        </ul>
                    </div>
                        
                </div>
            </article>
                
        </div>
    </div>

</section>

@stop


 


</div>
    
</body>
</html>