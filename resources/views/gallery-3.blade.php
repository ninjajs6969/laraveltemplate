<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 


</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Gallery Manasory</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="gallery-1">gallery</a>
                </li>
                
                <li>
                    Gallery Manasory
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="gallery sec-padd style-2 massonary-page">
    <div class="container">

        <div class="center">
            <ul class="post-filter style-2 list-inline">
                <li class="active" data-filter=".filter-item">
                    <span>View All</span>
                </li>
                <li data-filter=".Ecology">
                    <span>Ecology</span>
                </li>
                <li data-filter=".Wild-Animals">
                    <span>Wild Animals</span>
                </li>
                <li data-filter=".Recycling">
                    <span>Recycling</span>
                </li>
                <li data-filter=".Water">
                    <span>Water</span>
                </li>
                <li data-filter=".Pollution">
                    <span>Pollution</span>
                </li>
            </ul>
        </div>

        <div class="row filter-layout masonary-layout">

            
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/13.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/13.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Windmill Power</h4>
                        <p>Ecology, Recycling</p>
                        <div class="text">
                            <p>Natus error sit voluptatem accusan doloremque laudantium, totam rem aperiam, eaque sed ipsa quae ab illo inventore sunt explicabo...</p>
                        </div>
                    </div>
                </div>
            </article> 
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/14.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/14.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Wiliwili Recycling</h4>
                        <p>Recycling</p>
                        <div class="text">
                            <p>How all this mistaken idea  denouncing pleasure and praising pain was born and I will give you a complete account of the system...</p>
                        </div>
                    </div>
                </div>
            </article> 
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/15.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/15.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Video of Energy Power</h4>
                        <p>Pollution</p>
                        <div class="text">
                            <p>How all this mistaken idea  denouncing pleasure and praising pain was born and I will give you a complete account of the system...</p>
                        </div>
                    </div>
                </div>
            </article> 
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <iframe src="https://player.vimeo.com/video/9519939" width="370" height="235" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay></iframe>
                    </div>
                        
                    <div class="content">
                        <h4>Environment</h4>
                        <p>Pollution</p>
                        <div class="text">
                            <p>Natus error sit voluptatem accusan doloremque laudantium, totam rem aperiam, eaque sed ipsa quae ab illo inventore sunt explicabo...</p>
                        </div>
                    </div>
                </div>
            </article> 
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/16.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/16.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Natural Breathing</h4>
                        <p>Ecology, Recycling</p>
                        <div class="text">
                            <p>Natus error sit voluptatem accusan doloremque laudantium, totam rem aperiam, eaque sed ipsa quae ab illo inventore sunt explicabo...</p>
                        </div>
                    </div>
                </div>
            </article> 
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/17.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/17.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Save White Tiger</h4>
                        <p>Wild Animals</p>
                        <div class="text">
                            <p>How all this mistaken idea  denouncing pleasure and praising pain was born and I will give you a complete account of the system...</p>
                        </div>
                    </div>
                </div>
            </article> 
            
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/20.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/20.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Environment</h4>
                        <p>Pollution</p>
                        <div class="text">
                            <p>How all this mistaken idea  denouncing pleasure and praising pain was born and I will give you a complete account of the system...</p>
                        </div>
                    </div>
                </div>
            </article>  
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/19.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/19.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Educate Children</h4>
                        <p>Recycling</p>
                        <div class="text">
                            <p>Natus error sit voluptatem accusan doloremque laudantium, totam rem aperiam, eaque sed ipsa quae ab illo inventore sunt explicabo...</p>
                        </div>
                    </div>
                </div>
            </article> 
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/18.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/18.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Save White Tiger</h4>
                        <p>Wild Animals</p>
                        <div class="text">
                            <p>Natus error sit voluptatem accusan doloremque laudantium, totam rem aperiam, eaque sed ipsa quae ab illo inventore sunt explicabo...</p>
                        </div>
                    </div>
                </div>
            </article> 
            
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/22.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/22.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Protecting Wildlife</h4>
                        <p>Wild Animals</p>
                        <div class="text">
                            <p>Natus error sit voluptatem accusan doloremque laudantium, totam rem aperiam, eaque sed ipsa quae ab illo inventore sunt explicabo...</p>
                        </div>
                    </div>
                </div>
            </article> 
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/23.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/23.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Fresh Air Solution</h4>
                        <p>Water, Pollution</p>
                        <div class="text">
                            <p>Complete account of the system, and expound the actual teachings of the great explorer of the builder of human happiness...</p>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-md-4 col-sm-6 col-xs-12 filter-item Children">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/21.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/21.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="content">
                        <h4>Cooling Products</h4>
                        <p>Ecology</p>
                        <div class="text">
                            <p>Complete account of the system, and expound the actual teachings of the great explorer of the builder of human happiness...</p>
                        </div>
                    </div>
                </div>
            </article>  

 



        </div>
        <div class="center paddt-50"><a href="#" class="thm-btn">Load More</a></div>
    </div>
</section>
@stop

 




</div>
    
</body>
</html>