<!DOCTYPE html>
<html lang="en">
<head>
  
    <title>Eco Green || Responsive HTML 5 Template</title> 

</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Checkout</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li> 
                <li>
                    <a href="shop">shop</a>
                </li>                
                <li>
                    Checkout
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>




<section class="checkout-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="exisitng-customer">
                    <h5>Exisitng Customer?<a href="#">Click here to login</a></h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="coupon">
                    <h5>Have a Coupon?  <a href="#">Click here to enter your code</a></h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form billing-info">
                    <div class="section-title style-2">
                        <h3>Billing Address</h3>
                    </div>
                    <form method="post" action="checkout">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="field-label">Country*</div>
                                <div class="field-input">
                                    <input type="text" name="country" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-label">First Name*</div>
                                <div class="field-input">
                                    <input type="text" name="fname" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-label">Last Name *</div>
                                <div class="field-input">
                                    <input type="text" name="lname" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Address *</div>
                                <div class="field-input">
                                    <input type="text" name="address" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-input">
                                    <input type="text" name="address" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Town / City *</div>
                                <div class="field-input">
                                    <input type="text" name="town-city" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Contact Info *</div>
                                <div class="field-input">
                                    <input type="text" name="email" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-input">
                                    <input type="text" name="phone" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="create-acc">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="ship-same-address">
                                            <span>Create an Account</span>
                                        </label>
                                    </div>  
                                </div>
                            </div>
                       
                        </div>    
                    </form>
                </div>    
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form shipping-info">
                    <div class="section-title style-2">
                        <h3>Ship to Different Address<input type="checkbox" checked=""></h3>
                    </div>
                    <form method="post" action="checkout">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="field-label">Country*</div>
                                <div class="field-input">
                                    <input type="text" name="country" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-label">First Name*</div>
                                <div class="field-input">
                                    <input type="text" name="fname" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-label">Last Name *</div>
                                <div class="field-input">
                                    <input type="text" name="lname" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Address *</div>
                                <div class="field-input">
                                    <input type="text" name="address" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-input">
                                    <input type="text" name="address" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Town / City *</div>
                                <div class="field-input">
                                    <input type="text" name="town-city" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Other Notes</div>
                                <div class="field-input">
                                    <textarea name="other-notes" placeholder=""></textarea>
                                </div>
                            </div>
                            
                        </div>    
                    </form>
                </div>    
            </div>  
        </div>
        
        <div class="row bottom">
            <div class="col-lg-6 col-md-6">
                <div class="table">
                    <div class="section-title style-2">
                        <h3>Order Summary</h3>
                    </div>
                    <table class="cart-table">
                        <thead class="cart-header">
                            <tr>
                                <th class="product-column">Your Products</th>
                                <th>&nbsp;</th>
                                <th>Quantity</th>
                                <th class="price">Total</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2" class="product-column">
                                    <div class="column-box">
                                        <div class="prod-thumb">
                                            <a href="#"><img src="images/shop/11.jpg" alt=""></a>
                                        </div>
                                        <div class="product-title">
                                            <h3>The Innovators Dilemma</h3>
                                        </div>
                                    </div>
                                </td>
                                <td class="qty">
                                    <input class="quantity-spinner" type="text" value="2" name="quantity">
                                </td>
                                <td class="price">$34.00</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="product-column">
                                    <div class="column-box">
                                        <div class="prod-thumb">
                                            <a href="#"><img src="images/shop/12.jpg" alt=""></a>
                                        </div>
                                        <div class="product-title">
                                            <h3>Good to Great Look</h3>
                                        </div>
                                    </div>
                                </td>
                                <td class="qty">
                                    <input class="quantity-spinner" type="text" value="1" name="quantity">
                                </td>
                                <td class="price">$29.00</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="product-column">
                                    <div class="column-box">
                                        <div class="prod-thumb">
                                            <a href="#"><img src="images/shop/13.jpg" alt=""></a>
                                        </div>
                                        <div class="product-title">
                                            <h3>The Innovators Dilemma</h3>
                                        </div>
                                    </div>
                                </td>
                                <td class="qty">
                                    <input class="quantity-spinner" type="text" value="2" name="quantity">
                                </td>
                                <td class="price">$29.00</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="cart-total">
                    <div class="section-title style-2">
                        <h3>Cart Totals</h3>
                    </div>
                    <ul class="cart-total-table">
                        <li class="clearfix">
                            <span class="col col-title">Cart Subtotal</span>
                            <span class="col">$146.00</span>    
                        </li>
                        <li class="clearfix">
                            <span class="col col-title">Shipping and Handling</span>
                            <span class="col">$40.00- <b>Calculate Shipping</b></span>    
                        </li>
                        <li class="clearfix">
                            <span class="col col-title">Order Total</span>
                            <span class="col">$146.00</span>    
                        </li>      
                    </ul>
                    <div class="payment-options">
                        <div class="option-block">
                            <div class="checkbox">
                                <label>
                                    <input name="pay-us" type="checkbox">
                                    <span>Direct Bank Transfer</span>
                                </label>
                            </div>
                            <div class="text">
                                <p>Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
                            </div>
                        </div>

                        <div class="option-block">
                            <div class="radio-block">
                                <div class="checkbox">
                                    <label>
                                        <input name="pay-us" type="checkbox">
                                        <span>Paypal <b>What is Paypal</b></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="placeorder-button text-left">
                            <button type="submit" class="thm-btn">Place Order</button>
                        </div>   
                    </div>          
                </div>    
            </div>
             
        </div>
    </div>
</section>

@stop

 





</div>
    
</body>
</html>