<!DOCTYPE html>
<html lang="en">
<head>
    <title>Eco Green || Responsive HTML 5 Template</title> 

</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')

<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>My Account</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="shop">shop</a>
                </li>                
                <li>
                    My Account
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="register-section sec-padd-top">
    <div class="container">
        <div class="row">
            
            <!--Form Column-->
            <div class="form-column column col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
                <div class="section-title style-2">
                    <h3>Login Now</h3>
                </div>
                
                <!--Login Form-->
                <div class="styled-form login-form">
                    <form method="post" action="index">
                        <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-user"></span></span>
                            <input type="text" name="username" value="" placeholder="Your Name *">
                        </div>
                        <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-envelope-o"></span></span>
                            <input type="email" name="useremil" value="" placeholder="Enter Mail id *">
                        </div>
                        <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-unlock-alt"></span></span>
                            <input type="password" name="userpassword" value="" placeholder="Enter Password">
                        </div>
                        <div class="clearfix">
                            <div class="form-group pull-left">
                                <button type="button" class="thm-btn thm-tran-bg">login now</button>
                            </div>
                            <div class="form-group social-links-two padd-top-5 pull-right">
                                Or login with <a href="#" class="img-circle facebook"><span class="fa fa-facebook-f"></span></a> <a href="#" class="img-circle twitter"><span class="fa fa-twitter"></span></a> <a href="#" class="img-circle google-plus"><span class="fa fa-google-plus"></span></a>
                            </div>
                        </div>
                        
                        <div class="clearfix">
                            <div class="pull-left">
                                <input type="checkbox" id="remember-me"><label for="remember-me">&nbsp; Remember Me</label>
                            </div>
                        </div>
                        
                    </form>
                </div>
                
            </div>
            
            <!--Form Column-->
            <div class="form-column column col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <div class="section-title style-2">
                    <h3>Register Now</h3>
                </div>
                
                <!--Login Form-->
                <div class="styled-form register-form">
                    <form method="post" action="index">
                        <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-user"></span></span>
                            <input type="text" name="username" value="" placeholder="Your Name *">
                        </div>
                        <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-envelope-o"></span></span>
                            <input type="email" name="useremil" value="" placeholder="Enter Mail id *">
                        </div>
                        <div class="form-group">
                            <span class="adon-icon"><span class="fa fa-unlock-alt"></span></span>
                            <input type="password" name="userpassword" value="" placeholder="Enter Password">
                        </div>
                        <div class="clearfix">
                            <div class="form-group pull-left">
                                <button type="button" class="thm-btn thm-tran-bg">Register here</button>
                            </div>
                            <div class="form-group padd-top-15 pull-right">
                                * Free registered user to submit content.  
                            </div>
                        </div>
                        
                    </form>
                </div>
                
            </div>
            
        </div>
    </div>
</section>
@stop

 




</div>
    
</body>
</html>