<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 

</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Gallery Grid View</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="gallery-1">gallery</a>
                </li>
                
                <li>
                    Gallery Grid View
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>


<section class="gallery sec-padd style-2 grid-page">
    <div class="container">

        <ul class="post-filter style-2 list-inline float_left">
            <li class="active" data-filter=".filter-item">
                <span>View All</span>
            </li>
            <li data-filter=".Ecology">
                <span>Ecology</span>
            </li>
            <li data-filter=".Wild-Animals">
                <span>Wild Animals</span>
            </li>
            <li data-filter=".Recycling">
                <span>Recycling</span>
            </li>
            <li data-filter=".Water">
                <span>Water</span>
            </li>
            <li data-filter=".Pollution">
                <span>Pollution</span>
            </li>
        </ul>

        <div class="row filter-layout">

            
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/1.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/1.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Environment</h4>
                        <p>Pollution</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/2.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/2.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Windmill Power</h4>
                        <p>Ecology, Recycling</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/3.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/3.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Save White Tiger</h4>
                        <p>Wild Animals</p>
                    </div>
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/4.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/4.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Wiliwili Recycling</h4>
                        <p>Recycling</p>
                    </div>
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/5.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/5.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Plant Challenge</h4>
                        <p>Ecology</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Pollution Ecology Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/6.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/6.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Natural Breathing</h4>
                        <p>Ecology, Recycling</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/7.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/7.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Reduced Littering</h4>
                        <p>Water, Pollution</p>
                    </div>
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/8.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/8.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Innovation Effort</h4>
                        <p>Recycling</p>
                    </div>
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/9.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/9.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Protecting Nature</h4>
                        <p>Recycling, Water</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/10.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/10.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Fresh Air Solution</h4>
                        <p>Water, Pollution</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/11.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/11.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Cooling Products</h4>
                        <p>Ecology</p>
                    </div>
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/12.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/12.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Clean Power Plan</h4>
                        <p>Ecology</p>
                    </div>
                </div>
            </article>
        </div>
        <ul class="page_pagination center">
            <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
            <li><a href="#" class="active tran3s">1</a></li>
            <li><a href="#" class="tran3s">2</a></li>
            <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</section>



@stop







</div>
    
</body>
</html>