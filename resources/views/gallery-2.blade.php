<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 

 

</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')
@section('content')

<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Gallery Fullwidth</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="gallery-1">gallery</a>
                </li>
                
                <li>
                    Gallery Fullwidth
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="gallery sec-padd">
    <div class="container">
        <div class="section-title">
            <h2>Our Gallery</h2>
        </div>
        <ul class="post-filter list-inline">
            <li class="active" data-filter=".filter-item">
                <span><i class="fa fa-th-large"></i>View All</span>
            </li>
            <li data-filter=".Ecogreen">
                <span>Ecogreen</span>
            </li>
            <li data-filter=".Children">
                <span>Children</span>
            </li>
            <li data-filter=".Donate">
                <span>Donate</span>
            </li>
            <li data-filter=".Volunteer">
                <span>Volunteer</span>
            </li>
        </ul>

    
    </div>
    <div class="row filter-layout">

        
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Children">
            <div class="item">
                <img src="images/project/10.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/10.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article> 
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Donate">
            <div class="item">
                <img src="images/project/11.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/11.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article> 
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Ecogreen">
            <div class="item">
                <img src="images/project/12.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/12.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Volunteer">
            <div class="item">
                <img src="images/project/13.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/13.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Ecogreen Donate">
            <div class="item">
                <img src="images/project/14.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/14.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Children">
            <div class="item">
                <img src="images/project/15.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/15.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Ecogreen">
            <div class="item">
                <img src="images/project/16.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/16.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article> 
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Donate">
            <div class="item">
                <img src="images/project/17.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/17.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article> 
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Volunteer">
            <div class="item">
                <img src="images/project/18.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/18.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article> 
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Ecogreen Donate">
            <div class="item">
                <img src="images/project/19.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/19.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Donate">
            <div class="item">
                <img src="images/project/20.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/20.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article> 
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Donate">
            <div class="item">
                <img src="images/project/21.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/21.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article> 
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Volunteer">
            <div class="item">
                <img src="images/project/22.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/22.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Volunteer">
            <div class="item">
                <img src="images/project/23.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/23.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>
        <article class="col-md-3 col-5 col-sm-6 col-xs-12 filter-item Ecogreen Donate">
            <div class="item">
                <img src="images/project/24.jpg" alt="">
                <div class="overlay">
                    <div class="top">
                        <div class="box">
                            <div class="content">
                                <a data-group="1" href="images/project/24.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom"><a href="single-gallery"><h4>Cancer Children Network</h4></a></div>
                </div>
            </div>
        </article>



    </div>
    <div class="center paddt-50"><a href="#" class="thm-btn">Load More</a></div>
</section>

@stop

 


</div>
    
</body>
</html>