<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title>  

</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Campaigns Grid View</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="#">Pages</a>
                </li>
                
                <li>
                    Campaigns Grid View
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>


<section class="urgent-cause2 sec-padd">
    <div class="container">

        <div class="row">
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/resource/9.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Wind Power Grows Up</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $54000 </span></div>
                            <div class="donate float_right">Raised: $24000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>
            
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/resource/10.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Save White Tiger</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $92000 </span></div>
                            <div class="donate float_right">Raised: $69000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>    
            
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/resource/11.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Go Green Movement</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $78000 </span></div>
                            <div class="donate float_right">Raised: $49000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>

            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/cause/1.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Recycling Bamboo</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $54000 </span></div>
                            <div class="donate float_right">Raised: $24000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>
            
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/cause/2.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Kids Get Volunteers</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $92000 </span></div>
                            <div class="donate float_right">Raised: $69000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>    
            
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/cause/3.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Animal Feed</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $78000 </span></div>
                            <div class="donate float_right">Raised: $49000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>  

            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/cause/4.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">40,000 Elephants</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $54000 </span></div>
                            <div class="donate float_right">Raised: $24000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>
            
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/cause/5.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Wind Power Grows Up</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $92000 </span></div>
                            <div class="donate float_right">Raised: $69000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>    
            
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="item clearfix with-mb">
                    <figure class="img-box">
                        <img src="images/cause/6.jpg" alt="">
                        <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                    </figure>
                    
                    <div class="content">
                        
                        <div class="text center">
                            <a href="single-cause"><h4 class="title">Go Green Movements</h4></a>
                            <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                        </div>
                        <div class="progress-box">
                            <div class="bar">
                                <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                            </div>
                        </div>
                        <div class="donate clearfix">
                            <div class="donate float_left"><span>Goal: $78000 </span></div>
                            <div class="donate float_right">Raised: $49000</div>
                        </div>
                        
                    </div>                        
                            
                </div>
            </article>       
                
            
        </div>
        <ul class="page_pagination center">
            <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
            <li><a href="#" class="active tran3s">1</a></li>
            <li><a href="#" class="tran3s">2</a></li>
            <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</section>



@stop

 




</div>
    
</body>
</html>