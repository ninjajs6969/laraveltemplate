@extends('layouts._layout')

@section('content')
 <!--Start rev slider wrapper-->     
<section class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>
            
            <li data-transition="fade">
                <img src="images/slider/1.jpg"  alt="" width="1920" height="888" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="260" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="700">
                    <div class="slide-content-box">
                        <h1>Let’s<span>... go With ECO green</span></h1>
                        <h3>to get our global clean</h3>
                        <p>Great explorer of the truth, the master-builder of human happiness <br>not know how to pursue pleasure rationally...  </p>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="480" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="#">read more</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="200" 
                    data-y="top" data-voffset="480" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn style-3" href="cause">Get Involved</a>     
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="fade">
                <img src="images/slider/2.jpg"  alt="" width="1920" height="580" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="center" data-hoffset="" 
                    data-y="top" data-voffset="275" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="700">
                    <div class="slide-content-box center">
                        <h2>Protect the Wild animals</h2>
                        <p>Great explorer of the truth, the master-builder of human happiness <br>not know how to pursue pleasure rationally... </p>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="center" data-hoffset="-90" 
                    data-y="top" data-voffset="450" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="#">read more</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="center" data-hoffset="100" 
                    data-y="top" data-voffset="450" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn style-3" href="#">get involved</a>     
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="fade">
                <img src="images/slider/3.jpg"  alt="" width="1920" height="888" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="500" 
                    data-y="top" data-voffset="260" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="700">
                    <div class="slide-content-box">
                        <h1>Save<span>...World By Planting</span></h1>
                        <h3>trees Your Surrounding</h3>
                        <p>Great explorer of the truth, the master-builder of human happiness <br>not know how to pursue pleasure rationally...  </p>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="500" 
                    data-y="top" data-voffset="480" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn" href="#">read more</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="700" 
                    data-y="top" data-voffset="480" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn style-3" href="cause">Get Involved</a>     
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<!--End rev slider wrapper--> 


<section class="service sec-padd3">
    <div class="container">
        <div class="section-title center">
            <h2>We are ECO Green, Our Mission is <span class="thm-color">save water, animals and environment</span>our activities are taken around the world.</h2>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-x-12">
                <div class="service-item center">
                    <div class="icon-box">
                        <span class="icon-can"></span>
                    </div>
                    <h4>Recycling</h4>
                    <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-x-12">
                <div class="service-item center">
                    <div class="icon-box">
                        <span class="icon-tool"></span>
                    </div>
                    <h4>Eco System</h4>
                    <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-x-12">
                <div class="service-item center">
                    <div class="icon-box">
                        <span class="icon-nature-1"></span>
                    </div>
                    <h4>Save Water</h4>
                    <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-x-12">
                <div class="service-item center">
                    <div class="icon-box">
                        <span class="icon-deer"></span>
                    </div>
                    <h4>Save Animals</h4>
                    <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="fact-counter style-2 sec-padd" style="background-image: url(images/background/5.jpg);">
    <div class="container">
        <div class="section-title center">
            <h2>Some Interesting Facts</h2>
        </div>
        <div class="row clearfix">
            <div class="counter-outer clearfix">
                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="icon"><i class="icon-heart2"></i></div>
                        <div class="count-outer"><span class="count-text" data-speed="3000" data-stop="30">0</span>+</div>
                        <h4 class="counter-title">Year Of Experience</h4>
                    </div>
                        
                </article>
                
                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="icon"><i class="icon-money"></i></div>
                        <div class="count-outer">$<span class="count-text" data-speed="3000" data-stop="34500">0</span></div>
                        <h4 class="counter-title">Funds Collected</h4>
                    </div>
                </article>
                
                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="icon"><i class="icon-people3"></i></div>
                        <div class="count-outer"><span class="count-text" data-speed="3000" data-stop="347">0</span></div>
                        <h4 class="counter-title">Volunteers Involved</h4>
                    </div>
                </article>
                
                <!--Column-->
                <article class="column counter-column col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-duration="0ms">
                    <div class="item">
                        <div class="icon"><i class="icon-animals"></i></div>
                        <div class="count-outer"><span class="count-text" data-speed="3000" data-stop="485">0</span>%</div>
                        <h4 class="counter-title">Animals Saved</h4>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>





<section class="about sec-padd2">
    <div class="container">
        <div class="section-title center">
            <h2>Words About Us</h2>
            <p>Every voice counts! Choose campaign, donate and help us change the world</p>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <figure class="img-box">
                    <img src="images/resource/8.jpg" alt="">
                </figure>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="content">
                    <h2>Together we can make a difference</h2>
                    <div class="text">
                        <p>When you give to Our Ecogreen, you know your donation is making a difference. Whether you are supporting one of our Signature Programs or our carefully curated list of Gifts That Give More, our professional staff works hard every day <br>to ensure every dollar has impact for the cause of your choice. </p>
                    </div>
                    <h4>Our Partner</h4>
                    <div class="text">
                        <p>We partner with over 320 amazing projects worldwide, and have given over $150 million in cash and product grants to other groups since 2011. We also operate our own dynamic suite of Signature Programs.</p>
                    </div>
                    <div class="link"><a href="#" class="thm-btn style-2">Join With Us</a></div>
                </div>
            </div>
        </div>
        
    </div>
</section>



<section class="why-chooseus">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="inner-box">
                        <!--icon-box-->
                        <div class="icon_box">
                            <span class="icon-shapes"></span>
                        </div>
                        <a href="3"><h4>Supporting Good Cause</h4></a>
                    </div>
                    <div class="text"><p>Your contrbution used locally to help charitable causes and support the organization, Support only for good causes.  </p></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="inner-box">
                        <!--icon-box-->
                        <div class="icon_box">
                            <span class="icon-star"></span>
                        </div>
                        <a href="#"><h4>Most Trusted One</h4></a>
                    </div>
                    <div class="text"><p>Your contrbution used locally to help charitable causes and support the organization, Support only for good causes.  </p></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <div class="inner-box">
                        <!--icon-box-->
                        <div class="icon_box">
                            <span class="icon-people-1"></span>
                        </div>
                        <a href="#"><h4>Supporting Good Cause</h4></a>
                    </div>
                    <div class="text"><p>Your contrbution used locally to help charitable causes and support the organization, Support only for good causes.  </p></div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="gallery sec-padd3 style-2" style="background-image: url(images/background/8.jpg)";>
    <div class="container">
        <div class="section-title">
            <h2>Our Gallery</h2>
        </div>
        <ul class="post-filter style-3 list-inline float_right">
            <li class="active" data-filter=".filter-item">
                <span>View All</span>
            </li>
            <li data-filter=".Ecology">
                <span>Ecology</span>
            </li>
            <li data-filter=".Wild-Animals">
                <span>Wild Animals</span>
            </li>
            <li data-filter=".Recycling">
                <span>Recycling</span>
            </li>
            <li data-filter=".Water">
                <span>Water</span>
            </li>
            <li data-filter=".Pollution">
                <span>Pollution</span>
            </li>
        </ul>

        <div class="row filter-layout">

            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/1.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/1.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Environment</h4>
                        <p>Pollution</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/2.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/2.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Windmill Power</h4>
                        <p>Ecology, Recycling</p>
                    </div>
                </div>
            </article> 
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Ecology Recycling">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/3.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/3.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Save White Tiger</h4>
                        <p>Wild Animals</p>
                    </div>
                </div>
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12 filter-item Wild-Animals Pollution Water">
                <div class="item">
                    <div class="img-box">
                        <img src="images/project/4.jpg" alt="">
                        <div class="overlay">
                            <div class="inner-box">
                                <div class="content-box">
                                    <a data-group="1" href="images/project/4.jpg" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                    <a href="single-gallery"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content center">
                        <h4>Wiliwili Recycling</h4>
                        <p>Recycling</p>
                    </div>
                </div>
            </article>
        </div>

    </div>
</section>
 


<section class="urgent-cause2 sec-padd">
    <div class="container">
        <div class="section-title">
            <h2>Fundraising Campaigns</h2>
            <p>You can help lots of people by donating little.</p>
        </div>
        <div class="cause-carousel">
            <div class="item clearfix">
                <figure class="img-box">
                    <img src="images/resource/9.jpg" alt="">
                    <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                </figure>
                
                <div class="content">
                    
                    <div class="text center">
                        <a href="#"><h4 class="title">Wind Power Grows Up</h4></a>
                        <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                    </div>
                    <div class="progress-box">
                        <div class="bar">
                            <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                        </div>
                    </div>
                    <div class="donate clearfix">
                        <div class="donate float_left"><span>Goal: $54000 </span></div>
                        <div class="donate float_right">Raised: $24000</div>
                    </div>
                    
                </div>                        
                        
            </div>
            <div class="item clearfix">
                <figure class="img-box">
                    <img src="images/resource/10.jpg" alt="">
                    <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                </figure>
                
                <div class="content">
                    
                    <div class="text center">
                        <a href="#"><h4 class="title">Save White Tiger</h4></a>
                        <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                    </div>
                    <div class="progress-box">
                        <div class="bar">
                            <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                        </div>
                    </div>
                    <div class="donate clearfix">
                        <div class="donate float_left"><span>Goal: $92000 </span></div>
                        <div class="donate float_right">Raised: $69000</div>
                    </div>
                    
                </div>                        
                        
            </div>
            <div class="item clearfix">
                <figure class="img-box">
                    <img src="images/resource/11.jpg" alt="">
                    <div class="overlay"><div class="inner-box"><div class="content-box"><button class="thm-btn style-2 donate-box-btn">donate now</button></div></div></div>
                </figure>
                
                <div class="content">
                    
                    <div class="text center">
                        <a href="#"><h4 class="title">Go Green Movement</h4></a>
                        <p>We are dedicated to ending homelessness by delive- ring life-changing services for change the poor childrens life...</p>               
                    </div>
                    <div class="progress-box">
                        <div class="bar">
                            <div class="bar-inner animated-bar" data-percent="48%"><div class="count-text">48%</div></div>
                        </div>
                    </div>
                    <div class="donate clearfix">
                        <div class="donate float_left"><span>Goal: $78000 </span></div>
                        <div class="donate float_right">Raised: $49000</div>
                    </div>
                    
                </div>                        
                        
            </div>
            
        </div>
    </div>
</section>

<section class="event-style1" style="background-image: url(images/background/3.jpg)";>
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-10 col-xs-12">
                <div class="section-title">
                    <h2>UpComing Events</h2>
                </div>
            </div>
            <div class="col-md-3 col-sm-2 col-xs-12">
                <a href="#" class="thm-btn style-2 float_right">All Events</a>
            </div>
        </div>
        <div class="row">
            <article class="col-md-6 col-sm-12 col-xs-12">
                <div class="item style-1">
                    <div class="img-column">
                        <figure class="img-holder">
                            <a href="single-event"><img src="images/resource/1.jpg" alt=""></a>
                            <div class="date">21 <br><span>Mar</span></div>
                        </figure>
                    </div>
                    <div class="text-column">
                        <div class="lower-content">
                            <p>Organizer: Tom Maddy</p>
                            <a href="single-event"><h4>A Walk for Healthy Environment</h4></a>
                            <div class="text">
                                <p>Mauris tortor diam, laoreet quis commodo vitae, sodales vel augue.| Sed <br>rutrum, libero non pretium tristique, arcu mi sollicitudin...</p>               
                            </div>                            
                        </div>
                        <ul class="post-meta list_inline">
                            <li><i class="fa fa-clock-o"></i>Started On: 11.00am </li> |&nbsp;&nbsp;&nbsp;
                            <li><i class="fa fa-map-marker"></i> New Grand Street, California</li>
                        </ul>
                    </div>      
                </div>
            </article>
            <article class="col-md-6 col-sm-12 col-xs-12">
                <div class="item style-2">
                    <div class="clearfix">
                        <div class="img-column">
                            <figure class="img-holder">
                                <a href="single-event"><img src="images/resource/2.jpg" alt=""></a>
                            </figure>
                        </div>
                        <div class="text-column">
                            <div class="lower-content">
                                <p>Organizer: Tom Maddy</p>
                                <a href="single-event"><h4>Recycling Plastic Bottle</h4></a>
                                <div class="text">
                                    <p>Mauris tortor diam, laoreet quis commodo vitae, sodales vel augueed rutrum, libero non sed our pretium tristique, arcu mi sollicitudin...</p>               
                                </div>                            
                            </div>
                        </div> 
                    </div>
                    <ul class="post-meta list_inline">
                        <li><i class="fa fa-clock-o"></i>Started On: 11.00am </li>  |&nbsp;&nbsp;&nbsp;
                        <li><i class="fa fa-map-marker"></i> New Grand Street, California</li>
                    </ul>      
                </div>
                <div class="item style-2">
                    <div class="clearfix">
                        <div class="img-column">
                            <figure class="img-holder">
                                <a href="single-event"><img src="images/resource/3.jpg" alt=""></a>
                            </figure>
                        </div>
                        <div class="text-column">
                            <div class="lower-content">
                                <p>Organizer: Tom Maddy</p>
                                <a href="single-event"><h4>Green Construction Practice</h4></a>
                                <div class="text">
                                    <p>Mauris tortor diam, laoreet quis commodo vitae, sodales vel augueed rutrum, libero non sed our pretium tristique, arcu mi sollicitudin...</p>               
                                </div>                            
                            </div>
                        </div> 
                    </div> 
                    <ul class="post-meta list_inline">
                        <li><i class="fa fa-clock-o"></i>Started On: 11.00am </li> |&nbsp;&nbsp;&nbsp;
                        <li><i class="fa fa-map-marker"></i> New Grand Street, California</li>
                    </ul>     
                </div>
            </article>
        </div>
    </div>
</section>

<section class="blog-section sec-padd2">
    <div class="container">
        <div class="section-title center">
            <h2>latest news</h2>
        </div>
        <div class="row">
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/1.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">March 02, 2017</div>
                        <div class="post-meta">by fletcher  |  14 Comments</div>
                        <a href="blog-details"><h4>Steps for Save Animals</h4></a>
                        <div class="text">
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis sed praesentium voluptatum...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/2.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">January 14, 2017</div>
                        <div class="post-meta">by stephen  |  22 Comments</div>
                        <a href="blog-details"><h4>The Ozone Layer</h4></a>
                        <div class="text">
                            <p>How all this mistaken idea denouncing pleasure & praising pain was born and  will give you a complete...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/3.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">August 21, 2016</div>
                        <div class="post-meta">by Vincent  |  03 Comments</div>
                        <a href="blog-details"><h4>Dispose Plastic Products</h4></a>
                        <div class="text">
                            <p>The great explorer of the truth master builder of human happinessone rejects, dislikes, or avoids pleasure...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/4.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">July 15, 2016</div>
                        <div class="post-meta">by fletcher  |  14 Comments</div>
                        <a href="blog-details"><h4>Ideas for Save Energy</h4></a>
                        <div class="text">
                            <p>Know how to pursue pleasure rationally encounter consequences that extremely painful rationally encounter... </p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            
        </div>
    </div>
</section>

<div class="border-bottom"></div>

<section class="clients-section sec-padd">
    <div class="container">
        <div class="section-title center">
            <h2>our partners</h2>
        </div>
        <div class="client-carousel owl-carousel owl-theme">

            <div class="item tool_tip" title="media partner">
                <img src="images/clients/1.jpg" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/2.jpg" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/3.jpg" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/4.jpg" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/5.jpg" alt="Awesome Image">
            </div>

        </div>
    </div>  
</section>

@stop