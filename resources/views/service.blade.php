<!DOCTYPE html>
<html lang="en">
<head>
    
    <title>Eco Green || Responsive HTML 5 Template</title> 

    <!-- mobile responsive meta -->

</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')

@section('content')
   <div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Services</h1>
        </div>
    </div>
    </div>
    <div class="breadcumb-wrapper">
        <div class="container">
             <div class="pull-left">
                <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>                
                <li>
                    Services
                </li>
                </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>

<section class="default-section sec-padd">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-12">
                <div class="default-sidebar">
                    <ul class="service-catergory">
                        <li><a href="recycling">Recycling</a></li>
                        <li><a href="Eco-System">Eco System</a></li>
                        <li><a href="Save-Water">Save Water</a></li>
                        <li><a href="Save-Animals">Save Animals</a></li>
                        <li><a href="Organic-Living">Organic Living</a></li>
                        <li><a href="Good-Nature">Good Nature</a></li>
                    </ul>
                    <div class="link"><a href="#" class="thm-btn style-2">Become a Volunteer</a></div>
                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-12">
                <div class="service">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-x-12">
                            <div class="service-item center">
                                <div class="icon-box">
                                    <span class="icon-can"></span>
                                </div>
                                <h4>Recycling</h4>
                                <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-x-12">
                            <div class="service-item center">
                                <div class="icon-box">
                                    <span class="icon-tool"></span>
                                </div>
                                <h4>Eco System</h4>
                                <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-x-12">
                            <div class="service-item center">
                                <div class="icon-box">
                                    <span class="icon-nature-1"></span>
                                </div>
                                <h4>Save Water</h4>
                                <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-x-12">
                            <div class="service-item center">
                                <div class="icon-box">
                                    <span class="icon-deer"></span>
                                </div>
                                <h4>Save Animals</h4>
                                <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-x-12">
                            <div class="service-item center">
                                <div class="icon-box">
                                    <span class="icon-fruit"></span>
                                </div>
                                <h4>Save Animals</h4>
                                <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-x-12">
                            <div class="service-item center">
                                <div class="icon-box">
                                    <span class="icon-nature-2"></span>
                                </div>
                                <h4>Save Animals</h4>
                                <p>Praising pain was born & I will give  you a complete ac of the all systems, expound the actual great.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="feature sec-padd2">
    <div class="container">
        <div class="section-title">
            <h2>Our Philosophy</h2>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                
                <h4 class="thm-color">Natural Recources Defence Is Our Philosophy</h4><br>
                <p>We will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one avoids pleasure itself, because it is pleasure.</p><br>
                <p>Expound the actual teachings of the great explorer of the truth, the master-builder of human happiness, but because those who that are extremely painful. </p>
                <ul class="list-style-1">
                    <li><i class="fa fa-check-circle"></i>Sponsor meals for 50 children for 1 full year.</li>
                    <li><i class="fa fa-check-circle"></i>Sponsor mid-day meals for one month.</li>
                    <li><i class="fa fa-check-circle"></i>You can donate clothes, blankets and ect...</li>
                    <li><i class="fa fa-check-circle"></i>You can donate food material like rice, veggies.</li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-6">
                <h4>Video Presentation</h4><br>
                <iframe src="https://player.vimeo.com/video/9519939" width="570" height="310" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay></iframe>
            </div>
        </div><br><br>

    </div>
</div>

<section class="faq-section sec-padd2">
    <div class="container">
        <div class="section-title">
            <h2>Frequently Asked Questions</h2>
        </div>
        <div class="row">
            
            <div class="col-md-6 col-sm-12 col-xs-12">

                <div class="accordion-box style-one">
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What do you mean by item and end product</p>
                            <div class="toggle-icon">
                                <span class="plus fa fa-chevron-circle-right"></span><span class="minus fa fa-chevron-circle-down"></span>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals. 
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What are some examples of permitted end products?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">Am I allowed to modify the item that I purchased?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>

                </div>
    
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">

                <div class="accordion-box style-one">
                    
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What does non-exclusive mean?</p>
                            <div class="toggle-icon">
                                <span class="plus fa fa-chevron-circle-right"></span><span class="minus fa fa-chevron-circle-down"></span>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals. 
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What is a single application?</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>
                    <!--Start single accordion box-->
                    <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                        <div class="acc-btn">
                            <p class="title">What do you mean by item and end product</p>
                            <div class="toggle-icon">
                                <i class="plus fa fa-chevron-circle-right"></i><i class="minus fa fa-chevron-circle-down"></i>
                            </div>
                        </div>
                        <div class="acc-content">
                            <div class="text"><p>
                                Install a trunk guard at the base of the tree to keep works its seds nutrient and water system from being cut. Trunk ours guards also protect trees from rodents and other small animals.
                            </p></div>
                        </div>
                    </div>

                </div>

                    
            </div>

        </div>
    </div>
</section>
@stop



</div>
    
</body>
</html>