<!DOCTYPE html>
<html lang="en">
<head>

	<title>Eco Green || Responsive HTML 5 Template</title> 

</head>
<body>

<div class="boxed_wrapper">


@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>Blog Grid View</h1>
        </div>
    </div>
</div>
<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="blog-1">blog</a>
                </li>
                
                <li>
                    Blog Grid View
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>



<section class="all-blog blog-section sec-padd">
    <div class="container">
        <div class="row">
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/1.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">March 02, 2017</div>
                        <div class="post-meta">by fletcher  |  14 Comments</div>
                        <a href="blog-details"><h4>Steps for Save Animals</h4></a>
                        <div class="text">
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis sed praesentium voluptatum...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/2.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">January 14, 2017</div>
                        <div class="post-meta">by stephen  |  22 Comments</div>
                        <a href="blog-details"><h4>The Ozone Layer</h4></a>
                        <div class="text">
                            <p>How all this mistaken idea denouncing pleasure & praising pain was born and  will give you a complete...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/3.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">August 21, 2016</div>
                        <div class="post-meta">by Vincent  |  03 Comments</div>
                        <a href="blog-details"><h4>Dispose Plastic Products</h4></a>
                        <div class="text">
                            <p>The great explorer of the truth master builder of human happinessone rejects, dislikes, or avoids pleasure...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/4.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">July 15, 2016</div>
                        <div class="post-meta">by fletcher  |  14 Comments</div>
                        <a href="blog-details"><h4>Ideas for Save Energy</h4></a>
                        <div class="text">
                            <p>Know how to pursue pleasure rationally encounter consequences that extremely painful rationally encounter... </p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/5.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">August 21, 2016</div>
                        <div class="post-meta">by Vincent  |  03 Comments</div>
                        <a href="blog-details"><h4>Dispose Plastic Products</h4></a>
                        <div class="text">
                            <p>The great explorer of the truth master builder of human happinessone rejects, dislikes, or avoids pleasure...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/6.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">January 14, 2017</div>
                        <div class="post-meta">by stephen  |  22 Comments</div>
                        <a href="blog-details"><h4>Technology Integration</h4></a>
                        <div class="text">
                            <p>How all this mistaken idea denouncing pleasure & praising pain was born and  will give you a complete...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/7.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">August 21, 2016</div>
                        <div class="post-meta">by Vincent  |  03 Comments</div>
                        <a href="blog-details"><h4>My Zero Waste</h4></a>
                        <div class="text">
                            <p>The great explorer of the truth master builder of human happinessone rejects, dislikes, or avoids pleasure...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/8.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">January 14, 2017</div>
                        <div class="post-meta">by stephen  |  22 Comments</div>
                        <a href="blog-details"><h4>The Ozone Layer</h4></a>
                        <div class="text">
                            <p>How all this mistaken idea denouncing pleasure & praising pain was born and  will give you a complete...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/9.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">July 15, 2016</div>
                        <div class="post-meta">by fletcher  |  14 Comments</div>
                        <a href="blog-details"><h4>Ideas for Save Energy</h4></a>
                        <div class="text">
                            <p>Know how to pursue pleasure rationally encounter consequences that extremely painful rationally encounter... </p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/10.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">March 02, 2017</div>
                        <div class="post-meta">by fletcher  |  14 Comments</div>
                        <a href="blog-details"><h4>Steps for Save Animals</h4></a>
                        <div class="text">
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis sed praesentium voluptatum...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/11.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">January 14, 2017</div>
                        <div class="post-meta">by stephen  |  22 Comments</div>
                        <a href="blog-details"><h4>The Ozone Layer</h4></a>
                        <div class="text">
                            <p>How all this mistaken idea denouncing pleasure & praising pain was born and  will give you a complete...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>
            <article class="col-md-3 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details"><img src="images/blog/12.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">August 21, 2016</div>
                        <div class="post-meta">by Vincent  |  03 Comments</div>
                        <a href="blog-details"><h4>Reduce Your Waste</h4></a>
                        <div class="text">
                            <p>The great explorer of the truth master builder of human happinessone rejects, dislikes, or avoids pleasure...</p>               
                        </div>                        
                    </div>
                </div>
                
            </article>

            
        </div>
        <ul class="page_pagination center">
            <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
            <li><a href="#" class="active tran3s">1</a></li>
            <li><a href="#" class="tran3s">2</a></li>
            <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</section>

@stop


 





</div>
    
</body>
</html>