<meta charset="UTF-8">

	<!-- mobile responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/fav-icon/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" href="{{ asset('images/fav-icon/favicon-32x32.png') }}" sizes="32x32">
<link rel="icon" type="image/png" href="{{ asset('mages/fav-icon/favicon-16x16.png') }}" sizes="16x16">
