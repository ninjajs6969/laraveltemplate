<!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/menu.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mixitup.min.js') }}"></script>
    <script src="{{ asset('js/imagezoom.js') }}"></script> 
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script> 
    <script src="{{ asset('js/jquery.polyglot.language.switcher.js') }}"></script>
    <script src="{{ asset('js/SmoothScroll.js') }}"></script>
    <script src="{{ asset('js/jquery.appear.js') }}"></script>
    <script src="{{ asset('js/jquery.countTo.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script> 
    <script src="{{ asset('js/wow.js') }}"></script> 
    <script src="{{ asset('js/jquery.fitvids.js') }}"></script> 
    <script src="{{ asset('js/nouislider.js') }}"></script> 
    <script src="{{ asset('js/isotope.js') }}"></script> 
    <script src="{{ asset('js/pie-chart.js') }}"></script>

	<!-- revolution slider js -->
    <script src="{{ asset('js/rev-slider/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('js/rev-slider/revolution.extension.video.min.js') }}"></script>


    <script src="{{ asset('js/custom.js') }}"></script>