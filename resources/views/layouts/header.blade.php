<div class="top-bar">
    <div class="container">
        <div class="clearfix">

            <ul class="float_left top-bar-info">
                    <li><i class="icon-phone-call"></i>Phone: (123) 0200 12345</li>
                    <li><i class="icon-e-mail-envelope"></i>Supportus@Ecogreen.com</li>
                </ul>
            <div class="right-column float_right">
                <div id="polyglotLanguageSwitcher" class="">
                    <form action="#">
                        <select id="polyglot-language-options">
                            <option id="en" value="en" selected>Eng</option>
                            <option id="fr" value="fr">Fre</option>
                            <option id="de" value="de">Ger</option>
                            <option id="it" value="it">Ita</option>
                            <option id="es" value="es">Spa</option>
                        </select>
                    </form>
                </div>
                <ul class="social list_inline">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-feed"></i></a></li>
                </ul>
                <button class="thm-btn donate-box-btn">donate now</button>

            </div>
                
            
        </div>
            

    </div>
</div>

<section class="theme_menu stricky">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="main-logo">
                    <a href="/"><img src="images/logo/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-9 menu-column">
                <nav class="menuzord" id="main_menu">
                   <ul class="menuzord-menu">
                        <li><a href="/">Home</a></li>

                        <li><a href="service">Service</a>
                            <ul class="dropdown">
                                <li><a href="service">All Services</a></li>
                                <li><a href="recycling">Recycling</a></li>
                                <li><a href="Eco-System">Eco System</a></li>
                                <li><a href="Save-Water">Save Water</a></li>
                                <li><a href="Save-Animals">Save Animals</a></li>
                                <li><a href="Organic-Living">Organic Living</a></li>
                                <li><a href="Good-Nature">Good Nature</a></li>
                             </ul>
                        </li>

                        <li><a href="#">Pages</a>
                            <ul class="dropdown">
                                <li><a href="about">About Us</a></li>
                                <li><a href="Campaign-Grid-View">Campaign Grid View</a></li>
                                <li><a href="Campaign-List-View">Campaign List View</a></li>
                                <li><a href="Events">Events</a></li>
                                <li><a href="testimonial">Testimonials</a></li>
                                <li><a href="faq">FAQ’s</a></li>
                                <li><a href="error">404 Page</a></li>
                            </ul>
                        </li>

                        <li><a href="blog-1">blog</a>
                            <ul class="dropdown">
                                <li><a href="blog-1">Blog Grid View</a></li>
                                <li><a href="blog-large">Blog With Sidebar</a></li>
                                <li><a href="blog-details">Single Post</a></li>
                            </ul>
                        </li>

                        <li><a href="gallery-1">Gallery</a>
                        <ul class="dropdown">
                            <li><a href="gallery-1">gallery Grid View</a></li>
                            <li><a href="gallery-3">gallery manasory</a></li>
                            <li><a href="single-gallery">Single gallery</a></li>
                         </ul>
                        </li>

                        <li><a href="#">Shop</a>
                        <ul class="dropdown">
                            <li><a href="shop">Shop Products</a></li>
                            <li><a href="shop-single">Single Shop</a></li>
                            <li><a href="shop-cart">Shopping Cart</a></li>
                            <li><a href="checkout">Checkout</a></li>
                            <li><a href="account">My Account</a></li>
                        </ul>
                        </li>

                        <li><a href="contact">contact</a></li>


                    </ul>
                </nav> 
            </div>
            <div class="right-column">
                <div class="right-area">
                    <div class="nav_side_content">
                        <div class="search_option">
                            <button class="search tran3s dropdown-toggle color1_bg" id="searchDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i></button>
                            <form action="#" class="dropdown-menu" aria-labelledby="searchDropdown">
                                <input type="text" placeholder="Search...">
                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                       </div>
                   </div>
                </div>
                    
            </div>


        </div>
                

   </div>
</section>