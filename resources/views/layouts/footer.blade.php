<section class="call-out">
    <div class="container">
        <div class="float_left">
            <h4>Join Our Mission to Improve a Child's Feature, Pet’s Life and Our Planet.</h4>
        </div>
        <div class="float_right">
            <a href="#" class="thm-btn style-3">Get Involeved</a>
        </div>
                
    </div>
</section>
<footer class="main-footer">
    
    <!--Widgets Section-->
    <div class="widgets-section">
        <div class="container">
            <div class="row">
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        
                        <!--Footer Column-->
                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="footer-widget about-column">
                                <figure class="footer-logo"><a href="index"><img src="images/logo/logo2.png" alt=""></a></figure>
                                
                                <div class="text"><p>When you give to us you know your donation is making a diffe. </p> </div>
                                <ul class="contact-info">
                                    <li><span class="icon-signs"></span>22/121 Apple Street, New York, <br>NY 10012, USA</li>
                                    <li><span class="icon-phone-call"></span> Phone: +123-456-7890</li>
                                    <li><span class="icon-note"></span>Supportus@Eco greenteam.com</li>
                                </ul>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget link-column">
                                <div class="section-title">
                                    <h4>Quick Links</h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="about">About Our Eco green</a></li>
                                        <li><a href="Eco-System">Eco System</a></li>
                                        <li><a href="Organic-Living">Organic Living</a></li>
                                        <li><a href="Good-Nature">Good Nature</a></li>
                                        <li><a href="testimonial">Testimonials</a></li>
                                        <li><a href="Events">Upcoming Events</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Big Column--> 
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        
                        <!--Footer Column-->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget post-column">
                                <div class="section-title">
                                    <h4>Upcoming Events</h4>
                                </div>
                                <div class="post-list">
                                    <div class="post">
                                        <div class="post-thumb"><a href="#"><img src="images/blog/thumb1.jpg" alt=""></a></div>
                                        <a href="#"><h5>Marathon 2017: <br>Run for Cancer People</h5></a>
                                        <div class="post-info"><i class="fa fa-calendar"></i>  15 Mar, 2017</div>
                                    </div>
                                    <div class="post">
                                        <div class="post-thumb"><a href="#"><img src="images/blog/thumb2.jpg" alt=""></a></div>
                                        <a href="#"><h5>Let’s walk to the poor <br>children edu...</h5></a>
                                        <div class="post-info"><i class="fa fa-calendar"></i> 21 Apr, 2017</div>
                                    </div>

                                </div>
                                
                            </div>
                        </div>
                        
                        <!--Footer Column-->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget contact-column">
                                <div class="section-title">
                                    <h4>Subscribe Us</h4>
                                </div>
                                <h5>Subscribe to our newsletter!</h5>
                                <form action="#">
                                    <input type="email" placeholder="Email address....">
                                    <button type="submit"><i class="fa fa-paper-plane"></i></button>
                                </form>
                                <p>We don’t do mail to spam & your mail <br>id is confidential.</p>
                                
                                <ul class="social-icon">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-feed"></i></a></li>
                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                
             </div>
         </div>
     </div>
     
 
     
</footer>

<!--Footer Bottom-->
 <section class="footer-bottom">
    <div class="container">
        <div class="pull-left copy-text">
            <p><a href="#">Copyrights © 2017</a> All Rights Reserved. Powered by <a href="#">Eco green.</a></p>
            
        </div><!-- /.pull-right -->
        <div class="pull-right get-text">
            <a href="#">Join Us Now!</a>
        </div><!-- /.pull-left -->
    </div><!-- /.container -->
</section>

    <!-- Scroll Top  -->
	<button class="scroll-top tran3s color2_bg"><span class="fa fa-angle-up"></span></button>
	<!-- preloader  -->
	<div class="preloader"></div>
<div id="donate-popup" class="donate-popup">
    <div class="close-donate theme-btn"><span class="fa fa-close"></span></div>
    <div class="popup-inner">


    <div class="container">
        <div class="donate-form-area">
            <div class="section-title center">
                <h2>Donation Information</h2>
            </div>

            <h4>How much would you like to donate:</h4>

            <form  action="#" class="donate-form default-form">
                <ul class="chicklet-list clearfix">
                    <li>
                        <input type="radio" id="donate-amount-1" name="donate-amount" />
                        <label for="donate-amount-1" data-amount="1000" >$1000</label>
                    </li>
                    <li>
                        <input type="radio" id="donate-amount-2" name="donate-amount" checked="checked" />
                        <label for="donate-amount-2" data-amount="2000">$2000</label>
                    </li>
                    <li>
                        <input type="radio" id="donate-amount-3" name="donate-amount" />
                        <label for="donate-amount-3" data-amount="3000">$3000</label>
                    </li>
                    <li>
                        <input type="radio" id="donate-amount-4" name="donate-amount" />
                        <label for="donate-amount-4" data-amount="4000">$4000</label>
                    </li>
                    <li>
                        <input type="radio" id="donate-amount-5" name="donate-amount" />
                        <label for="donate-amount-5" data-amount="5000">$5000</label>
                    </li>
                    <li class="other-amount">

                        <div class="input-container" data-message="Every dollar you donate helps end hunger.">
                            <span>Or</span><input type="text" id="other-amount" placeholder="Other Amount"  />
                        </div>
                    </li>
                </ul>

                <h3>Donor Information</h3>

                <div class="form-bg">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            
                            <div class="form-group">
                                <p>Your Name*</p>
                                <input type="text" name="fname" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <p>Email*</p>
                                <input type="text" name="fname" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <p>Address*</p>
                                <input type="text" name="fname" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <p>Phn Num*</p>
                                <input type="text" name="fname" placeholder="">
                            </div>
                        </div>  

                    </div>
                </div>

                <ul class="payment-option">
                    <li>
                        <h4>Choose your payment method:</h4>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input name="pay-us" type="checkbox">
                                <span>Paypal</span>
                            </label>
                        </div>
                    </li>
                    <li>
                       <div class="checkbox">
                            <label>
                                <input name="pay-us" type="checkbox">
                                <span>Offline Donation</span>
                            </label>
                        </div> 
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input name="pay-us" type="checkbox">
                                <span>Credit Card</span>
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox">
                            <label>
                                <input name="pay-us" type="checkbox">
                                <span>Debit Card</span>
                            </label>
                        </div>
                    </li>
                </ul>

                <div class="center"><button class="thm-btn" type="submit">Donate Now</button></div>
                    
            
            </form>
        </div>
    </div>

            
        
    </div>
</div>