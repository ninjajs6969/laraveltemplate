<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 
    @include('layouts.styles')
	
</head>
<body>

<div class="boxed_wrapper">


@include('layouts/header')

@yield('content')
 
@include('layouts/footer')

		
@include('layouts/scripts')

</div>
	
</body>
</html>