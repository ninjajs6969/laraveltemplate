<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Eco Green || Responsive HTML 5 Template</title> 



</head>
<body>

<div class="boxed_wrapper">

@extends('layouts/_layout')
@section('content')
<div class="inner-banner has-base-color-overlay text-center" style="background: url(images/background/4.jpg);">
    <div class="container">
        <div class="box">
            <h1>404 Page</h1>
        </div>
    </div>
</div>

<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <ul class="list-inline link-list">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="#">Pages</a>
                </li>
                <li>
                    404 Page
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <a href="#" class="get-qoute"><i class="fa fa-arrow-circle-right"></i>Become a Volunteer</a>
        </div>
    </div>
</div>

<section class="not-found-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="img-box"><br><br><br>
                    <img src="images/resource/error.png" alt="">
                </div><br><br>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="not-found-content">
                    <h1>404</h1>
                    <h3>OOPPS! THE PAGE YOU WERE LOOKING FOR, COULDN'T BE FOUND.</h3>
                    <p>Try the search below to find matching pages:</p>
                    <form class="search-form" action="#">
                        <input placeholder="Search..." type="text">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@stop




</div>
    
</body>
</html>